# Translation of docs_krita_org_reference_manual___dockers___animation_curve.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___dockers___animation_curve\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-11 03:18+0200\n"
"PO-Revision-Date: 2019-06-11 08:28+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"

#: ../../reference_manual/dockers/animation_curve.rst:1
msgid "Overview of the animation curves docker."
msgstr "Огляд бічної панелі кривих анімації."

#: ../../reference_manual/dockers/animation_curve.rst:10
msgid "Animation"
msgstr "Анімація"

#: ../../reference_manual/dockers/animation_curve.rst:10
msgid "Animation Curves"
msgstr "Криві анімації"

#: ../../reference_manual/dockers/animation_curve.rst:10
msgid "Interpolation"
msgstr "Інтерполяція"

#: ../../reference_manual/dockers/animation_curve.rst:10
msgid "Tweening"
msgstr "Проміжні кадри"

#: ../../reference_manual/dockers/animation_curve.rst:15
msgid "Animation Curves Docker"
msgstr "Бічна панель кривих анімації"

#: ../../reference_manual/dockers/animation_curve.rst:17
msgid ""
"The Animation Curve docker allows you to edit tweened sections by means of "
"interpolation curves. As of this time of writing, it can only edit opacity."
msgstr ""
"За допомогою бічної панелі кривих анімації ви можете редагувати проміжні "
"кадри анімації на основі кривих інтерполяції (наближення). На момент "
"написання цього розділу можна було редагувати лише непрозорість."

#: ../../reference_manual/dockers/animation_curve.rst:19
msgid ""
"The idea is that sometimes what you want to animate can be expressed as a "
"value. This allows the computer to do maths on the values, and automate "
"tasks, like interpolation, also known as 'Tweening'. Because these are "
"values, like percentage opacity, and animation happens over time, that means "
"we can visualize the way the values are interpolated as a curve graph, and "
"also edit the graph that way."
msgstr ""
"Ідея полягає у тому, що іноді те, що ви хочете анімувати, можна виразити у "
"числовій формі. Це надає змогу комп'ютеру виконувати математичні дії зі "
"значеннями, автоматизувати завдання, зокрема інтерполяцію, також відому як "
"«створення проміжних кадрів». Оскільки це числові значення, зокрема відсоток "
"непрозорості, і анімація відбувається у часі, ми можемо візуалізувати спосіб "
"інтерполяції числових значень у формі графіка кривої, а також редагувати цей "
"графік."

#: ../../reference_manual/dockers/animation_curve.rst:21
msgid ""
"But, when you first open this docker, there's no curves visible! You will "
"first need to add opacity keyframes to the active animation layer. You can "
"do this by using the animation docker and selection :guilabel:`Add new "
"keyframe`."
msgstr ""
"Втім, коли ви вперше відкриєте цю бічну панель, не буде показано жодної "
"кривої! Вам слід спочатку додати непрозорі ключові кадри на активний шар "
"анімації. Зробити це можна за допомогою бічної панелі анімації і вибору "
"пункту :guilabel:`Додати новий ключовий кадр`."

#: ../../reference_manual/dockers/animation_curve.rst:25
msgid ".. image:: images/dockers/Animation_curves_1.png"
msgstr ".. image:: images/dockers/Animation_curves_1.png"

#: ../../reference_manual/dockers/animation_curve.rst:26
msgid ""
"Opacity should create a bright red curve line in the docker. On the left, in "
"the layer list, you will see that the active layer has an outline of its "
"properties: A red :guilabel:`Opacity` has appeared. Pressing the red dot "
"will hide the current curve, which'll be more useful in the future when more "
"properties can be animated."
msgstr ""
"Непрозорість має створити яскраву червону криву на бічній панелі. Ліворуч, у "
"списку шарів, ви побачите, що властивості активного шару буде взято у рамку: "
"з'явиться запис :guilabel:`Непрозорість` у червоній рамці. Натискання "
"червоної крапки приховає поточну криву — це буде корисною можливістю, коли, "
"у майбутньому, можливою буде анімація ширшого спектра властивостей."

#: ../../reference_manual/dockers/animation_curve.rst:29
msgid ".. image:: images/dockers/Animation_curves_2.png"
msgstr ".. image:: images/dockers/Animation_curves_2.png"

#: ../../reference_manual/dockers/animation_curve.rst:30
msgid ""
"If you select a dot of the curve, you can move it around to shift its place "
"in the time-line or its value."
msgstr ""
"Після позначення точки на кривій ви можете пересувати її з метою зміни "
"розташування на шкалі часу або значення."

#: ../../reference_manual/dockers/animation_curve.rst:32
msgid "On the top, you can select the method of smoothing:"
msgstr "У верхній частині ви можете вибрати метод згладжування:"

#: ../../reference_manual/dockers/animation_curve.rst:34
msgid "Hold Value"
msgstr "Утримувати значення"

#: ../../reference_manual/dockers/animation_curve.rst:35
msgid "This keeps the value the same until there's a new keyframe."
msgstr "Зберігає значення без змін, аж до нового ключового кадру."

#: ../../reference_manual/dockers/animation_curve.rst:36
msgid "Linear Interpolation (Default)"
msgstr "Лінійна інтерполяція (типовий варіант)"

#: ../../reference_manual/dockers/animation_curve.rst:37
msgid "This gives a straight interpolation between two values."
msgstr "Створює лінійну інтерполяцію між двома значеннями."

#: ../../reference_manual/dockers/animation_curve.rst:39
msgid "Custom interpolation"
msgstr "Нетипова інтерполяція"

#: ../../reference_manual/dockers/animation_curve.rst:39
msgid ""
"This allows you to set the section after the keyframe node as one that can "
"be modified. |mouseleft| +dragging on the node allows you to drag out a "
"handler node for adjusting the curving."
msgstr ""
"Це надасть вам змогу встановити переріз після вузла ключового кадру, "
"переріз, можна змінювати. Натискання |mouseleft| + перетягування вузла "
"надасть вам змогу скоригувати криву."

#: ../../reference_manual/dockers/animation_curve.rst:41
msgid ""
"So, for example, making a 100% opacity keyframe on frame 0 and a 0% opacity "
"one on frame 24 gives the following result:"
msgstr ""
"Отже, наприклад, встановлення 100% непрозорості для ключового кадру 0 і 0% "
"непрозорості для кадру 24 дає такий результат:"

#: ../../reference_manual/dockers/animation_curve.rst:44
msgid ".. image:: images/dockers/Ghost_linear.gif"
msgstr ".. image:: images/dockers/Ghost_linear.gif"

#: ../../reference_manual/dockers/animation_curve.rst:45
msgid ""
"If we select frame 12 and press :guilabel:`Add New Keyframe` a new opacity "
"keyframe will be added on that spot. We can set this frame to 100% and set "
"frame 0 to 0% for this effect."
msgstr ""
"Якщо вибрати кадр 12 і натиснути кнопку :guilabel:`Додати новий ключовий "
"кадр` буде додано новий ключовий кадр у відповідній позиції. Ми можемо "
"встановити для цього кадру 100% і встановити для кадру 0 значення 0% для "
"цього ефекту."

#: ../../reference_manual/dockers/animation_curve.rst:48
msgid ".. image:: images/dockers/Ghost_linear_in-out.gif"
msgstr ".. image:: images/dockers/Ghost_linear_in-out.gif"

#: ../../reference_manual/dockers/animation_curve.rst:49
msgid ""
"Now, if we want easing in, we select the node on frame 0 and press the :"
"guilabel:`Custom Interpolation` button at the top. This will enable custom "
"interpolation on the curve between frames 0 and 12. Doing the same on frame "
"12 will enable custom interpolation between frames 12 and 24. Drag from the "
"node to add a handle, which in turn you can use to get the following effects:"
msgstr ""
"Тепер, якщо нам потрібно наростання, ми виберемо вузол на кадрі 0 і "
"натиснете кнопку :guilabel:`Нетипова інтерполяція` у верхній частині панелі. "
"Це увімкне нетипову інтерполяцію на кривій між кадрами 0 і 12. Якщо зробити "
"те саме з кадром 12, буде увімкнено нетипову інтерполяцію між кадрами 12 і "
"24. Щоб додати нову точку керування, виконайте перетягування з вузла. Далі, "
"ви можете скористатися цим для отримання таких ефектів:"

#: ../../reference_manual/dockers/animation_curve.rst:52
msgid ".. image:: images/dockers/Ghost_ease_in-out.gif"
msgstr ".. image:: images/dockers/Ghost_ease_in-out.gif"

#: ../../reference_manual/dockers/animation_curve.rst:54
msgid ".. image:: images/dockers/Animation_curves_3.png"
msgstr ".. image:: images/dockers/Animation_curves_3.png"

#: ../../reference_manual/dockers/animation_curve.rst:55
msgid "The above shows an ease-in curve."
msgstr "Вище показано криву із наростанням."

#: ../../reference_manual/dockers/animation_curve.rst:57
msgid "And convex/concave examples:"
msgstr "І опуклі та увігнуті приклади:"

#: ../../reference_manual/dockers/animation_curve.rst:60
msgid ".. image:: images/dockers/Ghost_concave_in-out.gif"
msgstr ".. image:: images/dockers/Ghost_concave_in-out.gif"

#: ../../reference_manual/dockers/animation_curve.rst:62
msgid ".. image:: images/dockers/Animation_curves_4.png"
msgstr ".. image:: images/dockers/Animation_curves_4.png"

#: ../../reference_manual/dockers/animation_curve.rst:64
msgid ".. image:: images/dockers/Ghost_convex_int-out.gif"
msgstr ".. image:: images/dockers/Ghost_convex_int-out.gif"

#: ../../reference_manual/dockers/animation_curve.rst:66
msgid ".. image:: images/dockers/Animation_curves_5.png"
msgstr ".. image:: images/dockers/Animation_curves_5.png"

#: ../../reference_manual/dockers/animation_curve.rst:67
msgid ""
"As you may be able to tell, there's quite a different 'texture', so to "
"speak, to each of these animations, despite the difference being only in the "
"curves. Indeed, a good animator can get quite some tricks out of "
"interpolation curves, and as we develop Krita, we hope to add more "
"properties for you to animate this way."
msgstr ""
"Як можна бачити, існує різна «текстура», так би мовити, для кожної з цих "
"анімацій, хоча відмінність полягає лише у кривих. Насправді, кваліфікований "
"аніматор може досягти значних результатів за допомогою кривих інтерполяції. "
"З розвитком Krita ми сподіваємося додати інші властивості, які можна буде "
"анімувати у цей спосіб."

#: ../../reference_manual/dockers/animation_curve.rst:71
msgid ""
"Opacity has currently 255 as maximum in the curve editor, as that's how "
"opacity is stored internally."
msgstr ""
"У редакторі кривих значення непрозорості обмежено 255 через особливості "
"зберігання цього значення на внутрішньому рівні у програмі."
