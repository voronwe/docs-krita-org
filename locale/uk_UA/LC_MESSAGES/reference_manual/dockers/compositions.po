# Translation of docs_krita_org_reference_manual___dockers___compositions.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___dockers___compositions\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 11:26+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../<generated>:1
msgid "Rename composition"
msgstr "Перейменування композиції"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"

#: ../../reference_manual/dockers/compositions.rst:1
msgid "Overview of the compositions docker."
msgstr "Огляд бічної панелі композицій."

#: ../../reference_manual/dockers/compositions.rst:12
#: ../../reference_manual/dockers/compositions.rst:17
msgid "Compositions"
msgstr "Композиції"

#: ../../reference_manual/dockers/compositions.rst:19
msgid ""
"The compositions docker allows you to save the configurations of your layers "
"being visible and invisible, allowing you to save several configurations of "
"your layers."
msgstr ""
"За допомогою бічної панелі композиції ви можете зберігати налаштування "
"параметрі видимості і невидимості шарів, тобто створювати декілька різних "
"композицій шарів зображення."

#: ../../reference_manual/dockers/compositions.rst:22
msgid ".. image:: images/dockers/Composition-docker.png"
msgstr ".. image:: images/dockers/Composition-docker.png"

#: ../../reference_manual/dockers/compositions.rst:24
msgid "Adding new compositions"
msgstr "Додавання нових композицій"

#: ../../reference_manual/dockers/compositions.rst:24
msgid ""
"You do this by setting your layers as you wish, then pressing the plus sign. "
"If you had a word in the text-box to the left, this will be the name of your "
"new composition."
msgstr ""
"Зробити це можна позначенням бажаних пунктів шарів із наступним натисканням "
"кнопки із зображенням плюса. Якщо хочете, можете вказати якесь слово у полі "
"для введення тексту ліворуч. Вказане слово буде використано як назву вашої "
"композиції."

#: ../../reference_manual/dockers/compositions.rst:26
msgid "Activating composition"
msgstr "Активація композицій"

#: ../../reference_manual/dockers/compositions.rst:27
msgid "Double-click the composition name to switch to that composition."
msgstr ""
"Двічі клацніть на пункті назви композиції, щоб перемкнутися на цю композицію."

#: ../../reference_manual/dockers/compositions.rst:28
msgid "Removing compositions"
msgstr "Вилучення композицій"

#: ../../reference_manual/dockers/compositions.rst:29
msgid "The minus sign. Select a composition, and hit this button to remove it."
msgstr ""
"Кнопка зі знаком «мінус». Позначте пункт композиції і натисніть цю кнопку, "
"щоб її вилучити."

#: ../../reference_manual/dockers/compositions.rst:30
msgid "Exporting compositions"
msgstr "Експортування композицій"

#: ../../reference_manual/dockers/compositions.rst:31
msgid "The file sign. Will export all checked compositions."
msgstr "Значок теки. Експортує усі позначені композиції."

#: ../../reference_manual/dockers/compositions.rst:32
msgid "Updating compositions"
msgstr "Оновлення композицій"

#: ../../reference_manual/dockers/compositions.rst:33
msgid ""
"|mouseright| a composition to overwrite it with the current configuration."
msgstr ""
"Скористайтеся клацанням |mouseright| на пункті композиції, щоб перезаписати "
"її поточною конфігурацією."

#: ../../reference_manual/dockers/compositions.rst:35
msgid "|mouseright| a composition to rename it."
msgstr ""
"Скористайтеся клацанням |mouseright| на пункті композиції, щоб її "
"перейменувати."
