# Translation of docs_krita_org_reference_manual___tools___reference_images_tool.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___tools___reference_images_tool\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 11:49+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"

#: ../../<rst_epilog>:84
msgid ""
".. image:: images/icons/reference_images_tool.svg\n"
"   :alt: toolreference"
msgstr ""
".. image:: images/icons/reference_images_tool.svg\n"
"   :alt: toolreference"

#: ../../reference_manual/tools/reference_images_tool.rst:1
msgid "The reference images tool"
msgstr "Інструмент еталонних зображень"

#: ../../reference_manual/tools/reference_images_tool.rst:10
msgid "Tools"
msgstr "Інструменти"

#: ../../reference_manual/tools/reference_images_tool.rst:10
msgid "Reference"
msgstr "Довідник"

#: ../../reference_manual/tools/reference_images_tool.rst:15
msgid "Reference Images Tool"
msgstr "Інструмент еталонних зображень"

#: ../../reference_manual/tools/reference_images_tool.rst:17
msgid "|toolreference|"
msgstr "|toolreference|"

#: ../../reference_manual/tools/reference_images_tool.rst:21
msgid ""
"The reference images tool is a replacement for the reference images docker. "
"You can use it to load images from your disk as reference, which can then be "
"moved around freely on the canvas and placed wherever."
msgstr ""
"Інструмент еталонних зображень є замінником бічної панелі еталонних "
"зображень. Ви можете скористатися ним для завантаження еталонних зображень з "
"диска і наступного пересування цих зображень полотном."

#: ../../reference_manual/tools/reference_images_tool.rst:24
msgid "Tool Options"
msgstr "Параметри інструмента"

#: ../../reference_manual/tools/reference_images_tool.rst:26
msgid "Add reference image"
msgstr "Додати еталонне зображення"

#: ../../reference_manual/tools/reference_images_tool.rst:27
msgid "Load a single image to display on the canvas."
msgstr "Завантажити одинарне зображення для показу на полотні."

#: ../../reference_manual/tools/reference_images_tool.rst:28
msgid "Load Set"
msgstr "Завантажити набір"

#: ../../reference_manual/tools/reference_images_tool.rst:29
msgid "Load a set of reference images."
msgstr "Завантажити набір еталонних зображень."

#: ../../reference_manual/tools/reference_images_tool.rst:30
msgid "Save Set"
msgstr "Зберегти набір"

#: ../../reference_manual/tools/reference_images_tool.rst:31
msgid "Save a set of reference images."
msgstr "Зберегти набір еталонних зображень."

#: ../../reference_manual/tools/reference_images_tool.rst:32
msgid "Delete all reference images"
msgstr "Вилучити усі еталонні зображення"

#: ../../reference_manual/tools/reference_images_tool.rst:33
msgid "Delete all the reference images"
msgstr "Вилучити усі еталонні зображення"

#: ../../reference_manual/tools/reference_images_tool.rst:34
msgid "Keep aspect ratio"
msgstr "Зберігати співвідношення"

#: ../../reference_manual/tools/reference_images_tool.rst:35
msgid "When toggled this will force the image to not get distorted."
msgstr ""
"Якщо позначено, програма примусово запобігатиме викривленню зображення."

#: ../../reference_manual/tools/reference_images_tool.rst:36
msgid "Opacity"
msgstr "Непрозорість"

#: ../../reference_manual/tools/reference_images_tool.rst:37
msgid "Lower the opacity."
msgstr "Зменшити непрозорість."

#: ../../reference_manual/tools/reference_images_tool.rst:38
msgid "Saturation"
msgstr "Насиченість"

#: ../../reference_manual/tools/reference_images_tool.rst:39
msgid ""
"Desaturate the image. This is useful if you only want to focus on the light/"
"shadow instead of getting distracted by the colors."
msgstr ""
"Зменшити насиченість зображення. Корисно, якщо ви хочете зосередитися на "
"світлі і тінях, не відволікаючи увагу на кольори."

#: ../../reference_manual/tools/reference_images_tool.rst:41
msgid "How is the reference image stored."
msgstr "Спосіб збереження еталонного зображення."

#: ../../reference_manual/tools/reference_images_tool.rst:43
msgid "Embed to \\*.kra"
msgstr "Вбудувати до \\*.kra"

#: ../../reference_manual/tools/reference_images_tool.rst:44
msgid ""
"Store this reference image into the kra file. This is recommended for small "
"vital files you'd easily lose track of otherwise."
msgstr ""
"Зберегти це еталонне зображення до файла kra. Рекомендовано для малих "
"критично важливих файлів, які можуть легко загубитися."

#: ../../reference_manual/tools/reference_images_tool.rst:46
msgid "Storage mode"
msgstr "Режим зберігання"

#: ../../reference_manual/tools/reference_images_tool.rst:46
msgid "Link to external file."
msgstr "Посилання на зовнішній файл"

#: ../../reference_manual/tools/reference_images_tool.rst:46
msgid ""
"Only link to the reference image, krita will open it from the disk everytime "
"it loads this file. This is recommended for big files, or files that change "
"a lot."
msgstr ""
"Створити лише посилання на еталонне зображення. Krita відкриватиме еталонне "
"зображення з диска під час кожного завантаження цього файла. Рекомендовано "
"для великих файлів або для файлів, до яких часто вносять зміни."

#: ../../reference_manual/tools/reference_images_tool.rst:48
msgid ""
"You can move around reference images by selecting them with |mouseleft|, and "
"dragging them. You can rotate reference images by holding the cursor close "
"to the outside of the corners till the rotate cursor appears, while tilting "
"is done by holding the cursor close to the outside of the middle nodes. "
"Resizing can be done by dragging the nodes. You can delete a single "
"reference image by clicking it and pressing the :kbd:`Del` key. You can "
"select multiple reference images with the :kbd:`Shift` key and perform all "
"of these actions."
msgstr ""
"Ви можете пересувати еталонні зображення: позначте їх і скористайтеся "
"перетягуванням за допомогою |mouseleft|. Ви можете обертати еталонні "
"зображення, утримуючи курсор зовні зображення поблизу кутової точки, аж доки "
"не з'явиться курсор обертання. Перекошування виконується після появи курсора "
"перекошування у відповідь на утримання курсора поблизу середніх вузлів "
"обмежувальної рамки еталонного зображення. Змінити розміри зображення можна "
"перетягуванням вузлів. Вилучити окреме еталонне зображення можна клацанням "
"на ньому із наступним натисканням клавіші :kbd:`Del`. Позначити декілька "
"зображень можна, утримуючи клавішу :kbd:`Shift` під час клацання на "
"зображеннях. Над позначеною таким чином групою зображень можна виконувати "
"усі описані у цьому розділі дії."

#: ../../reference_manual/tools/reference_images_tool.rst:50
msgid ""
"To hide all reference images temporarily use :menuselection:`View --> Show "
"Reference Images`."
msgstr ""
"Щоб тимчасово приховати усі еталонні зображення, скористайтеся пунктом меню :"
"menuselection:`Перегляд --> Показати еталонні зображення`."
