# Vincent Pinon <vpinon@kde.org>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-03-12 00:04+0100\n"
"Last-Translator: Vincent Pinon <vpinon@kde.org>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 2.0\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"
"X-Language: fr_FR\n"
"X-Qt-Contexts: true\n"
"Generated-By: Babel 0.9.6\n"
"X-Source-Language: C\n"

#: ../../<rst_epilog>:36
msgid ""
".. image:: images/icons/freehand_path_tool.svg\n"
"   :alt: toolfreehandpath"
msgstr ""

#: ../../reference_manual/tools/freehand_path.rst:1
msgid "Krita's freehand path tool reference."
msgstr ""

#: ../../reference_manual/tools/freehand_path.rst:10
msgid "Tools"
msgstr ""

#: ../../reference_manual/tools/freehand_path.rst:10
msgid "Vector"
msgstr ""

#: ../../reference_manual/tools/freehand_path.rst:10
#, fuzzy
#| msgid "Freehand Path Tool"
msgid "Freehand"
msgstr "Tracé à main levée"

#: ../../reference_manual/tools/freehand_path.rst:15
msgid "Freehand Path Tool"
msgstr "Tracé à main levée"

#: ../../reference_manual/tools/freehand_path.rst:17
msgid "|toolfreehandpath|"
msgstr "|toolfreehandpath|"

#: ../../reference_manual/tools/freehand_path.rst:19
msgid ""
"With the Freehand Path Tool you can draw a path (much like the :ref:"
"`shape_brush_engine`) the shape will then be filled with the selected color "
"or pattern and outlined with a brush if so chosen. While drawing a preview "
"line is shown that can be modified in pattern, width and color."
msgstr ""

#: ../../reference_manual/tools/freehand_path.rst:21
msgid ""
"This tool can be particularly good for laying in large swaths of color "
"quickly."
msgstr ""
