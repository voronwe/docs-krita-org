msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_general_concepts___projection.pot\n"

#: ../../general_concepts/projection.rst:None
msgid ".. image:: images/category_projection/projection-cube_09.svg"
msgstr ""

#: ../../general_concepts/projection.rst:1
msgid "The Perspective Projection Category."
msgstr "透视投影文章分类。"

#: ../../general_concepts/projection.rst:15
msgid "Perspective Projection"
msgstr "透视投影原理"

#: ../../general_concepts/projection.rst:17
msgid ""
"The Perspective Projection tutorial is one of the Kickstarter 2015 tutorial "
"rewards. It's about something that humanity has known scientifically for a "
"very long time, and decent formal training will teach you about this. But I "
"think there are very very few tutorials about it in regard to how to achieve "
"it in digital painting programs, let alone open source."
msgstr ""
"透视投影教程是 2015 年 Kickstarter 的教程类回报项目。透视投影技术是一门历史悠"
"久的视觉科学技术，相关的知识不难找到。然而面向数字绘画软件的透视投影教程非常"
"少见，在开源协议下发布，面向开源软件的透视教程在当时也是不存在的。此系列教程"
"的填补了这个空白。"

#: ../../general_concepts/projection.rst:19
msgid ""
"The tutorial is a bit image heavy, and technical, but I hope the skill it "
"teaches will be really useful to anyone trying to get a grasp on a "
"complicated pose. Enjoy, and don't forget to thank `Raghukamath <https://www."
"raghukamath.com/>`_ for choosing this topic!"
msgstr ""

#: ../../general_concepts/projection.rst:24
msgid "Parts:"
msgstr "目录："
