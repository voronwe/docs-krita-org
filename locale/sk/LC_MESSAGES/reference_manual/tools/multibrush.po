# translation of docs_krita_org_reference_manual___tools___multibrush.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___tools___multibrush\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-04-02 13:24+0200\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 18.12.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../<rst_epilog>:2
#, fuzzy
#| msgid ""
#| ".. image:: images/icons/multibrush_tool.svg\n"
#| "   :alt: toolmultibrush"
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/multibrush_tool.svg\n"
"   :alt: toolmultibrush"

#: ../../<rst_epilog>:40
msgid ""
".. image:: images/icons/multibrush_tool.svg\n"
"   :alt: toolmultibrush"
msgstr ""
".. image:: images/icons/multibrush_tool.svg\n"
"   :alt: toolmultibrush"

#: ../../reference_manual/tools/multibrush.rst:None
#, fuzzy
#| msgid ".. image:: images/en/Krita-multibrush.png"
msgid ".. image:: images/tools/Krita-multibrush.png"
msgstr ".. image:: images/en/Krita-multibrush.png"

#: ../../reference_manual/tools/multibrush.rst:1
msgid "Krita's multibrush tool reference."
msgstr ""

#: ../../reference_manual/tools/multibrush.rst:11
#: ../../reference_manual/tools/multibrush.rst:31
msgid "Symmetry"
msgstr "Symetria"

#: ../../reference_manual/tools/multibrush.rst:11
msgid "Tools"
msgstr ""

#: ../../reference_manual/tools/multibrush.rst:11
#, fuzzy
#| msgid "Multibrush Tool"
msgid "Multibrush"
msgstr "Viacštetcový nástroj"

#: ../../reference_manual/tools/multibrush.rst:11
msgid "Mandala"
msgstr ""

#: ../../reference_manual/tools/multibrush.rst:11
msgid "Rotational Symmetry"
msgstr ""

#: ../../reference_manual/tools/multibrush.rst:16
msgid "Multibrush Tool"
msgstr "Viacštetcový nástroj"

#: ../../reference_manual/tools/multibrush.rst:18
msgid "|toolmultibrush|"
msgstr ""

#: ../../reference_manual/tools/multibrush.rst:20
msgid ""
"The Multibrush tool allows you to draw using multiple instances of a "
"freehand brush stroke at once, it can be accessed from the Toolbox docker or "
"with the default shortcut :kbd:`Q`. Using the Multibrush is similar to "
"toggling the :ref:`mirror_tools`, but the Multibrush is more sophisticated, "
"for example it can mirror freehand brush strokes along a rotated axis."
msgstr ""

#: ../../reference_manual/tools/multibrush.rst:22
msgid "The settings for the tool will be found in the tool options dock."
msgstr ""

#: ../../reference_manual/tools/multibrush.rst:24
msgid ""
"The multibrush tool has three modes and the settings for each can be found "
"in the tool options dock. Symmetry and mirror reflect over an axis which can "
"be set in the tool options dock. The default axis is the center of the "
"canvas."
msgstr ""

#: ../../reference_manual/tools/multibrush.rst:29
msgid "The three modes are:"
msgstr ""

#: ../../reference_manual/tools/multibrush.rst:32
msgid ""
"Symmetry will reflect your brush around the axis at even intervals. The "
"slider determines the number of instances which will be drawn on the canvas."
msgstr ""

#: ../../reference_manual/tools/multibrush.rst:33
msgid "Mirror"
msgstr "Zrkadliť"

#: ../../reference_manual/tools/multibrush.rst:34
msgid "Mirror will reflect the brush across the X axis, the Y axis, or both."
msgstr ""

#: ../../reference_manual/tools/multibrush.rst:35
msgid "Translate"
msgstr "Preložiť"

#: ../../reference_manual/tools/multibrush.rst:36
msgid ""
"Translate will paint the set number of instances around the cursor at the "
"radius distance."
msgstr ""

#: ../../reference_manual/tools/multibrush.rst:37
msgid "Snowflake"
msgstr "Snehová vločka"

#: ../../reference_manual/tools/multibrush.rst:38
msgid ""
"This works as a mirrored symmetry, but is a bit slower than symmetry+toolbar "
"mirror mode."
msgstr ""

#: ../../reference_manual/tools/multibrush.rst:40
#, fuzzy
#| msgid "Translate"
msgid "Copy Translate"
msgstr "Preložiť"

#: ../../reference_manual/tools/multibrush.rst:40
msgid ""
"This allows you to set the position of the copies relative to your own "
"cursor. To set the position of the copies, first toggle :guilabel:`Add`, and "
"then |mouseleft| the canvas to place copies relative to the multibrush "
"origin. Finally, press :guilabel:`Add` again, and start drawing to see the "
"copy translate in action."
msgstr ""

#: ../../reference_manual/tools/multibrush.rst:42
msgid ""
"The assistant and smoothing options work the same as in the :ref:"
"`freehand_brush_tool`, though only on the real brush and not its copies."
msgstr ""
