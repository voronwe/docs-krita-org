# Spanish translations for docs_krita_org_reference_manual___layers_and_masks.po package.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Eloy Cuadra <ecuadra@eloihr.net>, 2019.
# Sofia Priego <spriego@darksylvania.net>, %Y.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___layers_and_masks\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-07-14 13:40+0100\n"
"Last-Translator: Sofia Priego <spriego@darksylvania.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.2\n"

#: ../../reference_manual/layers_and_masks.rst:5
msgid "Layers and Masks"
msgstr "Capas y máscaras"

#: ../../reference_manual/layers_and_masks.rst:7
msgid "Layers are a central concept in digital painting."
msgstr "Las capas son un concepto principal del dibujo digital."

#: ../../reference_manual/layers_and_masks.rst:9
msgid ""
"With layers you can get better control over your artwork, for example you "
"can color an entire artwork just by working on the separate color layer and "
"thereby not destroying the line art which will reside above this color layer."
msgstr ""

#: ../../reference_manual/layers_and_masks.rst:11
msgid ""
"Furthermore, layers allow you to change the composition easier, and mass "
"transform certain elements at once."
msgstr ""

#: ../../reference_manual/layers_and_masks.rst:13
msgid ""
"Masks on the other hand allow you to selectively apply certain effects on a "
"layer, like transparency, transformation and filters."
msgstr ""

#: ../../reference_manual/layers_and_masks.rst:15
msgid "Check the :ref:`layers_and_masks` for more information."
msgstr ""
