# Spanish translations for docs_krita_org_user_manual___templates.po package.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Eloy Cuadra <ecuadra@eloihr.net>, 2019.
# Sofia Priego <spriego@darksylvania.net>, %Y.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_user_manual___templates\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-02-24 18:56+0100\n"
"Last-Translator: Sofia Priego <spriego@darksylvania.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 18.12.2\n"

#: ../../user_manual/templates.rst:1
msgid "How to use document templates in Krita."
msgstr "Cómo usar plantillas de documentos en Krita."

#: ../../user_manual/templates.rst:12
#, fuzzy
#| msgid "Templates"
msgid "Template"
msgstr "Plantillas"

#: ../../user_manual/templates.rst:17
msgid "Templates"
msgstr "Plantillas"

#: ../../user_manual/templates.rst:20
msgid ".. image:: images/Krita_New_File_Template_A.png"
msgstr ""

#: ../../user_manual/templates.rst:21
msgid ""
"Templates are just .kra files which are saved in a special location so it "
"can be pulled up by Krita quickly. This is like the :guilabel:`Open Existing "
"Document and Untitled Document` but then with a nicer place in the UI."
msgstr ""

#: ../../user_manual/templates.rst:23
msgid ""
"You can make your own template file from any .kra file, by using :guilabel:"
"`create template from image` in the file menu. This will add your current "
"document as a new template, including all its properties along with the "
"layers and layer contents."
msgstr ""

#: ../../user_manual/templates.rst:25
msgid "We have the following defaults:"
msgstr ""

#: ../../user_manual/templates.rst:28
msgid "Comic Templates"
msgstr "Plantillas de cómics"

#: ../../user_manual/templates.rst:30
msgid ""
"These templates are specifically designed for you to just get started with "
"drawing comics. The comic template relies on a system of vectors and clones "
"of those vector layers which automatically reflect any changes made to the "
"vector layers. In between these two, you can draw your picture, and not fear "
"them drawing over the panel. Use :guilabel:`Inherit Alpha` to clip the "
"drawing by the panel."
msgstr ""

#: ../../user_manual/templates.rst:32
msgid "European Bande Desinée Template."
msgstr ""

#: ../../user_manual/templates.rst:33
msgid ""
"This one is reminiscent of the system used by for example TinTin or Spirou "
"et Fantasio. These panels focus on wide images, and horizontal cuts."
msgstr ""

#: ../../user_manual/templates.rst:34
msgid "US-style comics Template."
msgstr ""

#: ../../user_manual/templates.rst:35
msgid ""
"This one is reminiscent of old DC and Marvel comics, such as Batman or "
"Captain America. Nine images for quick story progression."
msgstr ""

#: ../../user_manual/templates.rst:36
msgid "Manga Template."
msgstr "Plantilla para manga."

#: ../../user_manual/templates.rst:37
msgid ""
"This one is based on Japanese comics, and focuses on a thin vertical gutter "
"and a thick horizontal gutter, ensuring that the reader finished the "
"previous row before heading to the next."
msgstr ""

#: ../../user_manual/templates.rst:39
msgid "Waffle Iron Grid"
msgstr ""

#: ../../user_manual/templates.rst:39
msgid "12 little panels at your disposal."
msgstr ""

#: ../../user_manual/templates.rst:42
msgid "Design Templates"
msgstr "Diseño de plantillas"

#: ../../user_manual/templates.rst:44
msgid ""
"These are templates for design and have various defaults with proper ppi at "
"your disposal:"
msgstr ""

#: ../../user_manual/templates.rst:46
msgid "Cinema 16:10"
msgstr ""

#: ../../user_manual/templates.rst:47
msgid "Cinema 2.93:1"
msgstr ""

#: ../../user_manual/templates.rst:48
msgid "Presentation A3-landscape"
msgstr ""

#: ../../user_manual/templates.rst:49
msgid "Presentation A4 portrait"
msgstr ""

#: ../../user_manual/templates.rst:50
msgid "Screen 4:3"
msgstr "Pantalla 4:3"

#: ../../user_manual/templates.rst:51
msgid "Web Design"
msgstr "Diseño web"

#: ../../user_manual/templates.rst:54
msgid "DSLR templates"
msgstr "Plantillas DSLR"

#: ../../user_manual/templates.rst:56
msgid "These have some default size for photos"
msgstr ""

#: ../../user_manual/templates.rst:58
msgid "Canon 55D"
msgstr "Canon 55D"

#: ../../user_manual/templates.rst:59
msgid "Canon 5DMK3"
msgstr "Canon 5DMK3"

#: ../../user_manual/templates.rst:60
msgid "Nikon D3000"
msgstr "Nikon D3000"

#: ../../user_manual/templates.rst:61
msgid "Nikon D5000"
msgstr "Nikon D5000"

#: ../../user_manual/templates.rst:62
msgid "Nikon D7000"
msgstr "Nikon D7000"

#: ../../user_manual/templates.rst:65
#, fuzzy
#| msgid "Templates"
msgid "Texture Templates"
msgstr "Plantillas"

#: ../../user_manual/templates.rst:67
msgid "These are for making 3D textures, and are between 1024, to 4092."
msgstr ""
