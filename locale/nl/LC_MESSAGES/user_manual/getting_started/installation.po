# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-05-09 13:58+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../user_manual/getting_started/installation.rst:1
msgid "Detailed steps on how to install Krita"
msgstr "Gedetailleerde stappen over hoe Krita te installeren"

#: ../../user_manual/getting_started/installation.rst:14
#: ../../user_manual/getting_started/installation.rst:18
msgid "Installation"
msgstr "Installatie"

#: ../../user_manual/getting_started/installation.rst:21
msgid "Windows"
msgstr "Windows"

#: ../../user_manual/getting_started/installation.rst:22
msgid ""
"Windows users can download Krita from the website, the Windows Store, or "
"Steam."
msgstr ""

#: ../../user_manual/getting_started/installation.rst:24
msgid ""
"The versions on the Store and Steam cost money, but are `functionally "
"identical <https://krita.org/en/item/krita-available-from-the-windows-store/"
">`_ to the (free) website version. Unlike the website version, however, both "
"paid versions get automatic updates when new versions of Krita comes out. "
"After deduction of the Store fee, the purchase cost supports Krita "
"development."
msgstr ""

#: ../../user_manual/getting_started/installation.rst:31
msgid ""
"The latest version is always on our `website <https://krita.org/download/>`_."
msgstr ""

#: ../../user_manual/getting_started/installation.rst:33
msgid ""
"The page will try to automatically recommend the correct architecture (64- "
"or 32-bit), but you can select \"All Download Versions\" to get more "
"choices. To determine your computer architecture manually, go to :"
"menuselection:`Settings --> About`. Your architecture will be listed as the :"
"guilabel:`System Type` in the :guilabel:`Device Specifications` section."
msgstr ""

#: ../../user_manual/getting_started/installation.rst:35
msgid ""
"Krita by default downloads an **installer EXE**, but you can also download a "
"**portable zip-file** version instead. Unlike the installer version, this "
"portable version does not show previews in Windows Explorer automatically. "
"To get these previews with the portable version, also install Krita's "
"**Windows Shell Extension** extension (available on the download page)."
msgstr ""

#: ../../user_manual/getting_started/installation.rst:36
msgid "Website:"
msgstr "Website:"

#: ../../user_manual/getting_started/installation.rst:37
msgid ""
"These files are also available from the `KDE download directory <https://"
"download.kde.org/stable/krita/>`_."
msgstr ""

#: ../../user_manual/getting_started/installation.rst:38
msgid "Windows Store:"
msgstr "Windows-store:"

#: ../../user_manual/getting_started/installation.rst:39
msgid ""
"For a small fee, you can download Krita `from the Windows Store <https://www."
"microsoft.com/store/productId/9N6X57ZGRW96>`_. This version requires Windows "
"10."
msgstr ""

#: ../../user_manual/getting_started/installation.rst:41
msgid ""
"For a small fee, you can also download Krita `from Steam <https://store."
"steampowered.com/app/280680/Krita/>`_."
msgstr ""

#: ../../user_manual/getting_started/installation.rst:42
msgid "Steam:"
msgstr "Stoom:"

#: ../../user_manual/getting_started/installation.rst:44
msgid ""
"To download a portable version of Krita go to the `KDE <https://download.kde."
"org/stable/krita/>`_ download directory and get the zip-file instead of the "
"setup.exe installer."
msgstr ""

#: ../../user_manual/getting_started/installation.rst:48
msgid ""
"Krita requires Windows 7 or newer. The Store version requires Windows 10."
msgstr ""

#: ../../user_manual/getting_started/installation.rst:51
msgid "Linux"
msgstr "Linux"

#: ../../user_manual/getting_started/installation.rst:53
msgid ""
"Many Linux distributions package the latest version of Krita. Sometimes you "
"will have to enable an extra repository. Krita runs fine under most desktop "
"enviroments such as KDE, Gnome, LXDE, Xfce etc. -- even though it is a KDE "
"application and needs the KDE libraries. You might also want to install the "
"KDE system settings module and tweak the gui theme and fonts used, depending "
"on your distributions"
msgstr ""
"Veel Linux distributies bieden een pakket met de laatste versie van Krita. "
"Soms moet u een extra opslagruimte inschakelen. Krita werkt goed onder de "
"meeste bureaubladomgevingen, zoals KDE, Gnome, LXDE, Xfce etc. -- hoewel het "
"een KDE toepassing en de KDE bibliotheken nodig heeft. U zou misschien ook "
"de KDE module systeeminstellingen willen om het gebruikte gui-thema en "
"lettertype aan te passen, afhankelijk van uw distributies."

#: ../../user_manual/getting_started/installation.rst:61
msgid "Nautilus/Nemo file extensions"
msgstr "Nautilus/Nemo bestandsextensies"

#: ../../user_manual/getting_started/installation.rst:63
msgid ""
"Since April 2016, KDE's Dolphin file manager shows kra and ora thumbnails by "
"default, but Nautilus and it's derivatives need an extension. `We recommend "
"Moritz Molch's extensions for XCF, KRA, ORA and PSD thumbnails <https://"
"moritzmolch.com/1749>`__."
msgstr ""
"Sinds april 2016 toont bestandsbeheerder Dolphin van KDE standaard kra- en "
"ora-miniaturen, maar Nautilus en zijn afgeleiden hebben een extensie nodig. "
"`We bevelen de extensies van Moritz Molch voor XCF, KRA, ORA en PSD "
"miniaturen aan <https://moritzmolch.com/1749>`__."

#: ../../user_manual/getting_started/installation.rst:69
msgid "Appimages"
msgstr "App-images"

#: ../../user_manual/getting_started/installation.rst:71
msgid ""
"For Krita 3.0 and later, first try out the appimage from the website. **90% "
"of the time this is by far the easiest way to get the latest Krita.** Just "
"download the appimage, and then use the file properties or the bash command "
"chmod to make the appimage executable. Double click it, and enjoy Krita. (Or "
"run it in the terminal with ./appimagename.appimage)"
msgstr ""
"Voor Krita 3.0 en later, probeer de app-image op de website uit. **90% van "
"de tijd is dit veruit de gemakkelijkste manier om de laatste Krita op te "
"halen.** Download eenvoudig de app-image en gebruik daarna de "
"bestandseigenschappen of het bash-commando chmod om van de app-image een uit "
"te voeren bestand te maken. Dubbelklik erop en geniet van Krita. (Of voer "
"het uit in de terminal met ./appimagename.appimage)"

#: ../../user_manual/getting_started/installation.rst:78
msgid "Open the terminal into the folder you have the appimage."
msgstr "De terminal openen in de map waarin u de app-image heeft."

#: ../../user_manual/getting_started/installation.rst:79
msgid "Make it executable:"
msgstr "Het uitvoerbaar maken:"

#: ../../user_manual/getting_started/installation.rst:83
msgid "chmod a+x krita-3.0-x86_64.appimage"
msgstr ""

#: ../../user_manual/getting_started/installation.rst:85
msgid "Run Krita!"
msgstr "Krita uitvoeren!"

#: ../../user_manual/getting_started/installation.rst:89
msgid "./krita-3.0-x86_64.appimage"
msgstr ""

#: ../../user_manual/getting_started/installation.rst:91
msgid ""
"Appimages are ISOs with all the necessary libraries bundled inside, that "
"means no fiddling with repositories and dependencies, at the cost of a "
"slight bit more diskspace taken up (And this size would only be bigger if "
"you were using Plasma to begin with)."
msgstr ""
"App-images zijn ISO's met alle noodzakelijke bibliotheken erin, wat betekent "
"geen gedoe met opslagruimten en afhankelijkheden, ten koste van een heel "
"klein beetje meer gebruikte schijfruimte (en deze grootte zou alleen groter "
"zijn, om mee te beginnen, als u Plasma zou gebruiken)."

#: ../../user_manual/getting_started/installation.rst:97
msgid "Ubuntu and Kubuntu"
msgstr "Ubuntu en Kubuntu"

#: ../../user_manual/getting_started/installation.rst:99
msgid ""
"It does not matter which version of Ubuntu you use, Krita will run just "
"fine. However, by default, only a very old version of Krita is available. "
"You should either use the appimage, flatpak or the snap available from "
"Ubuntu's app store. We also maintain a ppa for getting latest builds of "
"Krita, you can read more about the ppa and install instructions `here "
"<https://launchpad.net/~kritalime/+archive/ubuntu/ppa>`_."
msgstr ""

#: ../../user_manual/getting_started/installation.rst:106
msgid "OpenSUSE"
msgstr "openSUSE"

#: ../../user_manual/getting_started/installation.rst:108
msgid "The latest stable builds are available from KDE:Extra repo:"
msgstr ""
"The laatste stabiele builds zijn beschikbaar uit de opslagruimte KDE:Extra:"

#: ../../user_manual/getting_started/installation.rst:110
msgid "https://download.opensuse.org/repositories/KDE:/Extra/"
msgstr "https://download.opensuse.org/repositories/KDE:/Extra/"

#: ../../user_manual/getting_started/installation.rst:113
msgid "Krita is also in the official repos, you can install it from Yast."
msgstr ""
"Krita is ook in de officiële opslagruimte, u kunt het vanuit Yast "
"installeren."

#: ../../user_manual/getting_started/installation.rst:116
msgid "Fedora"
msgstr "Fedora"

#: ../../user_manual/getting_started/installation.rst:118
msgid ""
"Krita is in the official repos, you can install it by using packagekit (Add/"
"Remove Software) or by writing the following command in terminal."
msgstr ""
"Krita is in de officiële opslagruimte, u kunt het installeren met packagekit "
"(Software toevoegen/verwijderen) of door het volgende commando in de "
"terminal te geven:"

#: ../../user_manual/getting_started/installation.rst:120
msgid "``dnf install krita``"
msgstr "``dnf install krita``"

#: ../../user_manual/getting_started/installation.rst:122
msgid ""
"You can also use the software center such as gnome software center or "
"Discover to install Krita."
msgstr ""

#: ../../user_manual/getting_started/installation.rst:125
msgid "Debian"
msgstr ""

#: ../../user_manual/getting_started/installation.rst:127
msgid ""
"The latest version of Krita available in Debian is 3.1.1. To install Krita "
"type the following line in terminal:"
msgstr ""

#: ../../user_manual/getting_started/installation.rst:130
msgid "``apt install krita``"
msgstr ""

#: ../../user_manual/getting_started/installation.rst:134
msgid "Arch"
msgstr "Arch"

#: ../../user_manual/getting_started/installation.rst:136
msgid ""
"Arch Linux provides krita package in the Extra repository. You can install "
"Krita by using the following command:"
msgstr ""

#: ../../user_manual/getting_started/installation.rst:139
msgid "``pacman -S krita``"
msgstr ""

#: ../../user_manual/getting_started/installation.rst:141
msgid ""
"You can also find Krita pkgbuild in arch user repositories but it is not "
"guaranteed to contain the latest git version."
msgstr ""

#: ../../user_manual/getting_started/installation.rst:145
msgid "OS X"
msgstr ""

#: ../../user_manual/getting_started/installation.rst:147
msgid ""
"You can download the latest binary from our `website <https://krita.org/"
"download/krita-desktop/>`__. The binaries work only with Mac OSX version "
"10.12 and newer."
msgstr ""

#: ../../user_manual/getting_started/installation.rst:152
msgid "Source"
msgstr "Bron"

#: ../../user_manual/getting_started/installation.rst:154
msgid ""
"While it is certainly more difficult to compile Krita from source than it is "
"to install from prebuilt packages, there are certain advantages that might "
"make the effort worth it:"
msgstr ""

#: ../../user_manual/getting_started/installation.rst:158
msgid ""
"You can follow the development of Krita on the foot. If you compile Krita "
"regularly from the development repository, you will be able to play with all "
"the new features that the developers are working on."
msgstr ""

#: ../../user_manual/getting_started/installation.rst:161
msgid ""
"You can compile it optimized for your processor. Most pre-built packages are "
"built for the lowest-common denominator."
msgstr ""

#: ../../user_manual/getting_started/installation.rst:163
msgid "You will be getting all the bug fixes as soon as possible as well."
msgstr ""

#: ../../user_manual/getting_started/installation.rst:164
msgid ""
"You can help the developers by giving us your feedback on features as they "
"are being developed and you can test bug fixes for us. This is hugely "
"important, which is why our regular testers get their name in the about box "
"just like developers."
msgstr ""

#: ../../user_manual/getting_started/installation.rst:169
msgid ""
"Of course, there are also some disadvantages: when building from the current "
"development source repository you also get all the unfinished features. It "
"might mean less stability for a while, or things shown in the user interface "
"that don't work. But in practice, there is seldom really bad instability, "
"and if it is, it's easy for you to go back to a revision that does work."
msgstr ""

#: ../../user_manual/getting_started/installation.rst:176
msgid ""
"So... If you want to start compiling from source, begin with the latest "
"build instructions from the guide :ref:`here <building_krita>`."
msgstr ""

#: ../../user_manual/getting_started/installation.rst:179
msgid ""
"If you encounter any problems, or if you are new to compiling software, "
"don't hesitate to contact the Krita developers. There are three main "
"communication channels:"
msgstr ""

#: ../../user_manual/getting_started/installation.rst:183
msgid "irc: irc.freenode.net, channel #krita"
msgstr ""

#: ../../user_manual/getting_started/installation.rst:184
msgid "`mailing list <https://mail.kde.org/mailman/listinfo/kimageshop>`__"
msgstr ""

#: ../../user_manual/getting_started/installation.rst:185
msgid "`forums <https://forum.kde.org/viewforum.php?f=136>`__"
msgstr ""

#~ msgid ""
#~ "Krita requires Windows Vista or newer. INTEL GRAPHICS CARD USERS: IF YOU "
#~ "SEE A BLACK OR BLANK WINDOW: UPDATE YOUR DRIVERS!"
#~ msgstr ""
#~ "Krita vereist Windows Vista of nieuwer. GEBRUIKERS VAN INTEL GRAFISCHE "
#~ "KAARTEN: ALS U EEN EEN ZWART OF BLANCO VENSTER ZIET: UPDATE UW DRIVERS!"

#~ msgid ""
#~ "Put here at the beginning, before we start on the many distro specific "
#~ "ways to get the program itself."
#~ msgstr ""
#~ "Hier aan het begin gezet, voordat we beginnen aan de vele specifieke "
#~ "manieren van distributies om het programma zelf op te halen."
