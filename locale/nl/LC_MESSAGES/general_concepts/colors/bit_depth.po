# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
# Lo Fernchu <lofernchu@zonnet.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-07-19 10:38+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.2\n"

#: ../../general_concepts/colors/bit_depth.rst:None
msgid ".. image:: images/color_category/Kiki_lowbit.png"
msgstr ".. image:: images/color_category/Kiki_lowbit.png"

#: ../../general_concepts/colors/bit_depth.rst:1
msgid "Bit depth in Krita."
msgstr "Bitdiepte in Krita."

#: ../../general_concepts/colors/bit_depth.rst:10
#: ../../general_concepts/colors/bit_depth.rst:15
msgid "Bit Depth"
msgstr "Bitdiepte"

#: ../../general_concepts/colors/bit_depth.rst:10
#: ../../general_concepts/colors/bit_depth.rst:26
msgid "Indexed Color"
msgstr "Geïndexeerde kleur"

#: ../../general_concepts/colors/bit_depth.rst:10
#: ../../general_concepts/colors/bit_depth.rst:46
msgid "Real Color"
msgstr "Echte kleur"

#: ../../general_concepts/colors/bit_depth.rst:10
msgid "Color"
msgstr "Kleur"

#: ../../general_concepts/colors/bit_depth.rst:10
msgid "Color Bit Depth"
msgstr "Bitdiepte kleur"

#: ../../general_concepts/colors/bit_depth.rst:10
msgid "Deep Color"
msgstr "Diepte kleur"

#: ../../general_concepts/colors/bit_depth.rst:10
msgid "Floating Point Color"
msgstr "Drijvende komma kleuren"

#: ../../general_concepts/colors/bit_depth.rst:10
msgid "Color Channels"
msgstr "Kleurkanalen"

#: ../../general_concepts/colors/bit_depth.rst:17
msgid ""
"Bit depth basically refers to the amount of working memory per pixel you "
"reserve for an image."
msgstr ""
"Bitdiepte verwijst in feite naar de hoeveelheid werkgeheugen per pixel die u "
"reserveert voor een afbeelding."

#: ../../general_concepts/colors/bit_depth.rst:19
msgid ""
"Like how having a A2 paper in real life can allow for much more detail in "
"the end drawing, it does take up more of your desk than a simple A4 paper."
msgstr ""
"Zoals het hebben van een A2 papiervel in het echt in staat stelt veel meer "
"detail in de eindtekening te stoppen, neemt het meer ruimte van uw bureau in "
"beslag dan een eenvoudig A4 papierblad."

#: ../../general_concepts/colors/bit_depth.rst:21
msgid ""
"However, this does not just refer to the size of the image, but also how "
"much precision you need per color."
msgstr ""
"Dit verwijst niet alleen naar de grootte van de afbeelding, maar ook hoeveel "
"precisie u nodig hebt per kleur."

#: ../../general_concepts/colors/bit_depth.rst:23
msgid ""
"To illustrate this, I'll briefly talk about something that is not even "
"available in Krita:"
msgstr ""
"Om dit te illustreren zal ik het kort hebben over iets dat zelfs niet in "
"Krita beschikbaar is:"

#: ../../general_concepts/colors/bit_depth.rst:28
msgid ""
"In older programs, the computer would have per image, a palette that "
"contains a number for each color. The palette size is defined in bits, "
"because the computer can only store data in bit-sizes."
msgstr ""
"In oudere programma's had de computer per afbeelding, een palet waarin voor "
"elke kleur een was opgeslagen. De paletgrootte is gedefinieerd in bits, "
"omdat de computer gegevens alleen in bit-groottes kan opslaan."

#: ../../general_concepts/colors/bit_depth.rst:36
msgid "1bit"
msgstr "1 bit"

#: ../../general_concepts/colors/bit_depth.rst:37
msgid "Only two colors in total, usually black and white."
msgstr "Slechts twee kleuren in totaal, gewoonlijk zwart en wit."

#: ../../general_concepts/colors/bit_depth.rst:38
msgid "4bit (16 colors)"
msgstr "4bits (16 kleuren)"

#: ../../general_concepts/colors/bit_depth.rst:39
msgid ""
"16 colors in total, these are famous as many early games were presented in "
"this color palette."
msgstr ""
"16 kleuren in totaal, deze zijn beroemd omdat veel vroege spellen "
"gepresenteerd werden in dit kleurpalet."

#: ../../general_concepts/colors/bit_depth.rst:41
msgid "8bit"
msgstr "8 bits"

#: ../../general_concepts/colors/bit_depth.rst:41
msgid ""
"256 colors in total. 8bit images are commonly used in games to save on "
"memory for textures and sprites."
msgstr ""
"256 kleuren in totaal. 8 bit afbeeldingen worden in spellen veel gebruikt om "
"minder geheugen te gebruiken voor textures en sprites."

#: ../../general_concepts/colors/bit_depth.rst:43
msgid ""
"However, this is not available in Krita. Krita instead works with channels, "
"and counts how many colors per channel you need (described in terms of "
"''bits per channel''). This is called 'real color'."
msgstr ""
"Maar dit is niet beschikbaar in Krita. In plaats daarvan werkt Krita met "
"kanalen, en telt hoeveel kleuren u per kanaal nodig heeft (Beschreven in "
"termen als ''bits per channel''). Dit heet 'Echte kleur'."

#: ../../general_concepts/colors/bit_depth.rst:52
msgid ".. image:: images/color_category/Rgbcolorcube_3.png"
msgstr ".. image:: images/color_category/Rgbcolorcube_3.png"

#: ../../general_concepts/colors/bit_depth.rst:52
msgid ""
"1, 2, and 3bit per channel don't actually exist in any graphics application "
"out there, however, by imagining them, we can imagine how each bit affects "
"the precision: Usually, each bit subdivides each section in the color cube "
"meaning precision becomes a power of 2 bigger than the previous cube."
msgstr ""
"Eigenlijk komen 1, 2, en 3 bits per kanaal in geen enkel grafisch programma "
"voor, maar, door het ons zich voor te stellen, kunnen we het ons voorstellen "
"hoe elk bit de precisie beïnvloedt: Normaal gesproken deelt elke bit elke "
"sectie van de kleuren kubus wat inhoud dat de precisie een factor 2 groter "
"is dan in de vorige kubus."

#: ../../general_concepts/colors/bit_depth.rst:54
msgid "4bit per channel (not supported by Krita)"
msgstr "4 bits per kanaal (niet ondersteund door Krita)"

#: ../../general_concepts/colors/bit_depth.rst:55
msgid ""
"Also known as Hi-Color, or 16bit color total. A bit of an old system, and "
"not used outside of specific displays."
msgstr ""
"Ook bekend als Hi-color, of 16bit color total. Een beetje uit de oude doos, "
"en buiten enkele specifieke displays niet meer in gebruik. "

#: ../../general_concepts/colors/bit_depth.rst:56
msgid "8bit per channel"
msgstr "8 bits per kanaal"

#: ../../general_concepts/colors/bit_depth.rst:57
msgid ""
"Also known as \"True Color\", \"Millions of colors\" or \"24bit/32bit\". The "
"standard for many screens, and the lowest bit-depth Krita can handle."
msgstr ""
"Ook bekend als \"True Color\", \"Millions of colors\" of \"24bit/32bit\". De "
"standaard voor veel beeldschermen, en de kleinste bitdiepte dat Krita kan "
"hanteren."

#: ../../general_concepts/colors/bit_depth.rst:58
msgid "16bit per channel"
msgstr "16 bits per kanaal"

#: ../../general_concepts/colors/bit_depth.rst:59
msgid ""
"One step up from 8bit, 16bit per channel allows for colors that can't be "
"displayed by the screen. However, due to this, you are more likely to have "
"smoother gradients. Sometimes known as \"Deep Color\". This color depth type "
"doesn't have negative values possible, so it is 16bit precision, meaning "
"that you have 65536 values per channel."
msgstr ""
"Een stap hoger dan 8 bit, 16 bit per kanaal zijn kleuren mogelijk die niet "
"op een monitor zichtbaar zijn. Maar, hierdoor heeft u waarschijnlijk "
"gladdere kleurverlopen. Dit staat soms bekend als \"Deep Color\". Bij dit "
"soort kleurdiepte zijn geen negatieve waarden mogelijk, dit is dus 16 bit "
"precisie, wat inhoud dat u 65536 waarden per kanaal heeft."

#: ../../general_concepts/colors/bit_depth.rst:60
msgid "16bit float"
msgstr "16 bits drijvende komma"

#: ../../general_concepts/colors/bit_depth.rst:61
msgid ""
"Similar to 16bit, but with more range and less precision. Where 16bit only "
"allows coordinates like [1, 4, 3], 16bit float has coordinates like [0.15, "
"0.70, 0.3759], with [1.0,1.0,1.0] representing white. Because of the "
"differences between floating point and integer type variables, and because "
"Scene-referred imaging allows for negative values, you have about 10-11bits "
"of precision per channel in 16 bit floating point compared to 16 bit in 16 "
"bit int (this is 2048 values per channel in the 0-1 range). Required for HDR/"
"Scene referred images, and often known as 'half floating point'."
msgstr ""
"Vergelijkbaar met 16 bit, maar met een groter bereik en minder precisie. "
"Waar 16 bit alleen coördinaten zoals [1, 4, 3] mogelijk zijn , heeft 16 bit "
"drijvende komma coördinaten zoals [0.15, 0.70, 0.3759], waarbij "
"[1.0,1.0,1.0] wit voorstelt. Vanwege de verschillen tussen de variabele van "
"het type drijvende komma en integer, en in omdat Scene-referred afbeeldingen "
"negatieve waarden toegestaan zijn, heeft u ongeveer 10-11 bits precisie per "
"kanaal bij 16 bit drijvende komma vergeleken met 16 bit in 16 bit int (dit "
"zijn 2048 waarden per kanaal in de 0-1 bereik). Vereist voor HDR/Scene "
"referred afbeeldingen, en vaak bekend als 'half floating point'."

#: ../../general_concepts/colors/bit_depth.rst:63
msgid ""
"Similar to 16bit float but with even higher precision. The native color "
"depth of OpenColor IO, and thus faster than 16bit float in HDR images, if "
"not heavier. Because of the nature of floating point type variables, 32bit "
"float is roughly equal to 23-24 bits of precision per channel (16777216 "
"values per channel in the 0-1 range), but with a much wider range (it can go "
"far above 1), necessary for HDR/Scene-referred values. It is also known as "
"'single floating point'."
msgstr ""
"Vergelijkbaar met 16 bit drijvende komma maar met een nog hogere precisie. "
"Het is de eigen kleurdiepte van OpenColor IO, en is dus sneller dan 16 bit "
"drijvende komma in HDR afbeeldingen, maar niet zwaarder. Vanwege de "
"eigenschappen van drijvende komma type variabelen, is 32 bit drijvende komma "
"ongeveer gelijk aan 23-24 bits precisie per kanaal (16777216 waarden per "
"kanaal in de 0-1 range), maar met een veel groter bereik (het kan ver boven "
"de 1 gaan), wat noodzakelijk is bij HDR/Scene-referred waarden. Het staat "
"ook bekend als 'single floating point'."

#: ../../general_concepts/colors/bit_depth.rst:64
msgid "32bit float"
msgstr "32 bits drijvende komma"

#: ../../general_concepts/colors/bit_depth.rst:66
msgid ""
"This is important if you have a working color space that is larger than your "
"device space: At the least, if you do not want color banding."
msgstr ""
"Deze is belangrijk als u een kleurruimte in gebruik heeft die groter is dan "
"uw apparaatruimte: tenminste, als u geen color banding wilt."

#: ../../general_concepts/colors/bit_depth.rst:68
msgid ""
"And while you can attempt to create all your images a 32bit float, this will "
"quickly take up your RAM. Therefore, it's important to consider which bit "
"depth you will use for what kind of image."
msgstr ""
"En als u wil proberen om al uw afbeeldingen in 32 bit drijvende komma te "
"bewerken, zal dit heel snel al uw RAM in beslag nemen. Het is daarom "
"belangrijk om te overwegen hoeveel bitdiepte u wilt gebruiken voor welk "
"soort afbeelding."
