# Translation of docs_krita_org_general_concepts___colors___color_models.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: general_concepts\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-08-05 10:28+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.07.70\n"

#: ../../general_concepts/colors/color_models.rst:1
msgid "Color Models in Krita"
msgstr "Models de color en el Krita"

#: ../../general_concepts/colors/color_models.rst:10
#: ../../general_concepts/colors/color_models.rst:15
msgid "Color Models"
msgstr "Models de color"

#: ../../general_concepts/colors/color_models.rst:10
msgid "Color"
msgstr "Color"

#: ../../general_concepts/colors/color_models.rst:17
msgid ""
"Krita has many different color spaces and models. Following here is a brief "
"explanation of each, and their use-cases."
msgstr ""
"El Krita té molts espais de color i models diferents. A continuació hi ha "
"una breu explicació de cadascun, i els seus casos d'ús."

#: ../../general_concepts/colors/color_models.rst:22
msgid "RGB"
msgstr "RGB"

#: ../../general_concepts/colors/color_models.rst:24
msgid "Red, Green, Blue."
msgstr "Vermell, verd i blau."

#: ../../general_concepts/colors/color_models.rst:26
msgid ""
"These are the most efficient primaries for light-based color mixing, like "
"computer screens. Adding Red, Green and Blue light together results in "
"White, and is thus named the additive color wheel."
msgstr ""
"Aquests són els primaris més eficients per a la mescla del colors basada en "
"la llum, com les pantalles d'ordinador. L'addició de llum Vermella, Verda i "
"Blava dóna com a resultat el color Blanc, de manera que s'anomena roda de "
"color additiva."

#: ../../general_concepts/colors/color_models.rst:28
msgid "RGB is used for two purposes:"
msgstr "El RGB s'utilitza per a dos propòsits:"

#: ../../general_concepts/colors/color_models.rst:30
msgid "Images that are meant for viewing on a screen:"
msgstr "Imatges destinades a ser visualitzades en una pantalla:"

#: ../../general_concepts/colors/color_models.rst:32
msgid ""
"So that could be images for the web, buttons, avatars, or just portfolio "
"images."
msgstr ""
"De manera que aquestes podrien ser imatges per a la web, botons, avatars o "
"simplement per a un portafolis."

#: ../../general_concepts/colors/color_models.rst:33
msgid "Or for Video games, both sprites and textures are best in RGB there."
msgstr ""
"O per als videojocs, tant les franges com les textures són millors en RGB."

#: ../../general_concepts/colors/color_models.rst:34
msgid "Or for 3d rendering, visual effects and cg animation."
msgstr ""
"O per a la renderització en 3D, efectes visuals i animació per ordinador."

#: ../../general_concepts/colors/color_models.rst:36
msgid ""
"And for the working space. A working space is an RGB gamut that is really "
"large and predictable, meaning it's good for image manipulation. You use "
"this next to a profiled monitor. This way you can have precise colors while "
"also being able to view them correctly on multiple screens."
msgstr ""
"I per a l'espai de treball. Un espai de treball és una gamma RGB que en "
"realitat és gran i predictible, el qual vol dir que és bo per a la "
"manipulació d'imatges. L'utilitzeu al costat d'un monitor amb perfil. "
"D'aquesta manera, podreu tenir colors precisos al mateix temps que els "
"veureu correctament en múltiples pantalles."

#: ../../general_concepts/colors/color_models.rst:39
msgid "Blending modes in RGB"
msgstr "Modes de barreja en RGB"

#: ../../general_concepts/colors/color_models.rst:43
#: ../../general_concepts/colors/color_models.rst:85
#: ../../general_concepts/colors/color_models.rst:102
msgid "Color 1"
msgstr "Color 1"

#: ../../general_concepts/colors/color_models.rst:43
#: ../../general_concepts/colors/color_models.rst:85
#: ../../general_concepts/colors/color_models.rst:102
msgid "Color 2"
msgstr "Color 2"

#: ../../general_concepts/colors/color_models.rst:43
#: ../../general_concepts/colors/color_models.rst:85
#: ../../general_concepts/colors/color_models.rst:102
msgid "Normal"
msgstr "Normal"

#: ../../general_concepts/colors/color_models.rst:43
#: ../../general_concepts/colors/color_models.rst:85
#: ../../general_concepts/colors/color_models.rst:102
msgid "Multiply"
msgstr "Multiplica"

#: ../../general_concepts/colors/color_models.rst:43
#: ../../general_concepts/colors/color_models.rst:85
#: ../../general_concepts/colors/color_models.rst:102
msgid "Screen"
msgstr "Pantalla"

#: ../../general_concepts/colors/color_models.rst:45
msgid "R"
msgstr "Vr"

#: ../../general_concepts/colors/color_models.rst:45
#: ../../general_concepts/colors/color_models.rst:87
msgid "G"
msgstr "V"

#: ../../general_concepts/colors/color_models.rst:45
msgid "B"
msgstr "B"

#: ../../general_concepts/colors/color_models.rst:47
#: ../../general_concepts/colors/color_models.rst:106
msgid "R & G"
msgstr "V i G"

#: ../../general_concepts/colors/color_models.rst:47
#: ../../general_concepts/colors/color_models.rst:106
msgid "1.0"
msgstr "1,0"

#: ../../general_concepts/colors/color_models.rst:47
#: ../../general_concepts/colors/color_models.rst:106
#: ../../general_concepts/colors/color_models.rst:108
#: ../../general_concepts/colors/color_models.rst:121
msgid "0.0"
msgstr "0,0"

#: ../../general_concepts/colors/color_models.rst:47
#: ../../general_concepts/colors/color_models.rst:49
#: ../../general_concepts/colors/color_models.rst:89
#: ../../general_concepts/colors/color_models.rst:106
#: ../../general_concepts/colors/color_models.rst:108
#: ../../general_concepts/colors/color_models.rst:119
#: ../../general_concepts/colors/color_models.rst:121
msgid "0.5"
msgstr "0,5"

#: ../../general_concepts/colors/color_models.rst:49
#: ../../general_concepts/colors/color_models.rst:89
#: ../../general_concepts/colors/color_models.rst:108
msgid "Gray"
msgstr "Gris"

#: ../../general_concepts/colors/color_models.rst:49
#: ../../general_concepts/colors/color_models.rst:89
#: ../../general_concepts/colors/color_models.rst:106
#: ../../general_concepts/colors/color_models.rst:108
#: ../../general_concepts/colors/color_models.rst:119
#: ../../general_concepts/colors/color_models.rst:121
msgid "0.25"
msgstr "0,25"

#: ../../general_concepts/colors/color_models.rst:49
#: ../../general_concepts/colors/color_models.rst:89
#: ../../general_concepts/colors/color_models.rst:106
#: ../../general_concepts/colors/color_models.rst:108
#: ../../general_concepts/colors/color_models.rst:119
#: ../../general_concepts/colors/color_models.rst:121
msgid "0.75"
msgstr "0,75"

#: ../../general_concepts/colors/color_models.rst:55
msgid "RGB models: HSV, HSL, HSI and HSY"
msgstr "Models RGB: HSV, HSL, HSI i HSY"

#: ../../general_concepts/colors/color_models.rst:57
msgid ""
"These are not included as their own color spaces in Krita. However, they do "
"show up in the blending modes and color selectors, so a brief overview:"
msgstr ""
"Aquests no estan inclosos com els seus propis espais de color en el Krita. "
"No obstant això, es mostren en els modes de barreja i selectors de color, de "
"manera que en farem una breu descripció:"

#: ../../general_concepts/colors/color_models.rst:59
msgid "--Images of relationship rgb-hsv etc."
msgstr "--Imatges de relació rgb-hsv, etc."

#: ../../general_concepts/colors/color_models.rst:61
msgid "Hue"
msgstr "To"

#: ../../general_concepts/colors/color_models.rst:62
msgid ""
"The tint of a color, or, whether it's red, yellow, green, etc. Krita's Hue "
"is measured in 360 degrees, with 0 being red, 120 being green and 240 being "
"blue."
msgstr ""
"El to d'un color, o si és vermell, groc, verd, etc. El To en el Krita es "
"mesura en 360 graus, amb 0 per al vermell, 120 per al verd i 240 per al blau."

#: ../../general_concepts/colors/color_models.rst:63
msgid "Saturation"
msgstr "Saturació"

#: ../../general_concepts/colors/color_models.rst:64
msgid ""
"How vibrant a color is. Saturation is slightly different between HSV and the "
"others. In HSV it's a measurement of the difference between two base colors "
"being used and three base colors being used. In the others it's a "
"measurement of how close a color is to gray, and sometimes this value is "
"called **Chroma**. Saturation ranges from 0 (gray) to 100 (pure color)."
msgstr ""
"Com de vibrant serà un color. La Saturació és lleugerament diferent entre "
"HSV i els altres. En HSV és una mesura de la diferència entre dos colors "
"base que s'utilitzen i tres colors base que s'utilitzen. En els altres, és "
"una mesura de com de prop es troba un color del gris, i algunes vegades "
"aquest valor s'anomena **Chroma**. L'interval de la saturació varia entre el "
"0 (gris) fins al 100 (color pur)."

#: ../../general_concepts/colors/color_models.rst:65
msgid "Value"
msgstr "Valor"

#: ../../general_concepts/colors/color_models.rst:66
msgid ""
"Sometimes known as Brightness. Measurement of how much the pixel needs to "
"light up. Also measured from 0 to 100."
msgstr ""
"De vegades conegut com a Brillantor. Mesura quant cal il·luminar el píxel. "
"També es mesura des de 0 fins a 100."

#: ../../general_concepts/colors/color_models.rst:67
msgid "Lightness"
msgstr "Claredat"

#: ../../general_concepts/colors/color_models.rst:68
msgid ""
"Where a color aligns between white and black. This value is non-linear, and "
"puts all the most saturated possible colors at 50. Ranges from 0 to 100."
msgstr ""
"On un color s'alinea entre el blanc i el negre. Aquest valor no és lineal, i "
"posarà tots els colors més saturats possibles al 50. L'interval va des de 0 "
"fins al 100."

#: ../../general_concepts/colors/color_models.rst:69
msgid "Intensity"
msgstr "Intensitat"

#: ../../general_concepts/colors/color_models.rst:70
msgid ""
"Similar to lightness, except it acknowledges that yellow (1,1,0) is lighter "
"than blue (0,0,1). Ranges from 0 to 100."
msgstr ""
"Similar a la claredat, excepte que reconeix que el groc (1,1,0) és més clar "
"que el blau (0,0,1). L'interval va des del 0 fins al 100."

#: ../../general_concepts/colors/color_models.rst:72
msgid "Luma (Y')"
msgstr "Luma (Y')"

#: ../../general_concepts/colors/color_models.rst:72
msgid ""
"Similar to lightness and Intensity, except it weights the red, green and "
"blue components based real-life measurements of how much light a color "
"reflects to determine its lightness. Ranges from 0 to 100. Luma is well "
"known for being used in film-color spaces."
msgstr ""
"Similar a la claredat i la intensitat, excepte que pesa els components "
"vermell, verd i blau en funció de les mesures a la vida real de la quantitat "
"de llum que reflecteix un color per a determinar la seva claredat. "
"L'interval va des del 0 fins al 100. El Luma és ben conegut pel seu ús en "
"els espais de color de les pel·lícules."

#: ../../general_concepts/colors/color_models.rst:77
msgid "Grayscale"
msgstr "Escala de grisos"

#: ../../general_concepts/colors/color_models.rst:79
msgid ""
"This color space only registers gray values. This is useful, because by only "
"registering gray values, it only needs one channel of information, which in "
"turn means the image becomes much lighter in memory consumption!"
msgstr ""
"Aquest espai de color només registra els valors de gris. Això és útil, ja "
"que en només registrar els valors de gris, només necessitarà un canal "
"d'informació, el que al seu torn voldrà dir que la imatge esdevindrà molt "
"més baixa en el consum de memòria."

#: ../../general_concepts/colors/color_models.rst:82
msgid ""
"This is useful for textures, but also anything else that needs to stay "
"grayscale, like Black and White comics."
msgstr ""
"És útil per a les textures, però també per a qualsevol altra cosa que calgui "
"mantenir en escala de grisos, com els còmics en blanc i negre."

#: ../../general_concepts/colors/color_models.rst:95
msgid "CMYK"
msgstr "CMYK"

#: ../../general_concepts/colors/color_models.rst:97
msgid "Cyan, Magenta, Yellow, Key"
msgstr "Cian, Magenta, Groc i Clau"

#: ../../general_concepts/colors/color_models.rst:99
msgid ""
"This is the color space of printers. Unlike computers, printers have these "
"four colors, and adding them all adds up to black instead of white. This is "
"thus also called a 'subtractive' color space."
msgstr ""
"Aquest és l'espai de color de les impressores. A diferència dels ordinadors, "
"les impressores tenen aquests quatre colors, i en afegir-los tots s'afegirà "
"el negre en lloc del blanc. Per tant, aquest també s'anomena un espai de "
"color «substractiu»."

#: ../../general_concepts/colors/color_models.rst:104
#: ../../general_concepts/colors/color_models.rst:117
msgid "C"
msgstr "C"

#: ../../general_concepts/colors/color_models.rst:104
#: ../../general_concepts/colors/color_models.rst:117
msgid "M"
msgstr "M"

#: ../../general_concepts/colors/color_models.rst:104
#: ../../general_concepts/colors/color_models.rst:117
#: ../../general_concepts/colors/color_models.rst:165
msgid "Y"
msgstr "G"

#: ../../general_concepts/colors/color_models.rst:104
#: ../../general_concepts/colors/color_models.rst:117
msgid "K"
msgstr "Cl"

#: ../../general_concepts/colors/color_models.rst:111
msgid ""
"There's also a difference between 'colored gray' and 'neutral gray' "
"depending on the profile."
msgstr ""
"També hi ha una diferència entre la «coloració grisa» i el «gris neutre» "
"segons el perfil."

#: ../../general_concepts/colors/color_models.rst:115
msgid "25%"
msgstr "25%"

#: ../../general_concepts/colors/color_models.rst:115
msgid "50%"
msgstr "50%"

#: ../../general_concepts/colors/color_models.rst:115
msgid "75%"
msgstr "75%"

#: ../../general_concepts/colors/color_models.rst:119
msgid "Colored Gray"
msgstr "Coloració grisa"

#: ../../general_concepts/colors/color_models.rst:121
msgid "Neutral Gray"
msgstr "Gris neutral"

#: ../../general_concepts/colors/color_models.rst:128
msgid ".. image:: images/color_category/Cmyk_black_differences.png"
msgstr ".. image:: images/color_category/Cmyk_black_differences.png"

#: ../../general_concepts/colors/color_models.rst:128
msgid ""
"In Krita, there's also the fact that the default color is a perfect black in "
"RGB, which then gets converted to our default CMYK in a funny manner, giving "
"a yellow look to the strokes. Again, another good reason to work in RGB and "
"let the conversion be done by the printing house."
msgstr ""
"En el Krita, també hi ha el fet que el color predeterminat és un negre "
"perfecte en RGB, el qual després es convertirà al nostre CMYK predeterminat "
"en una manera divertida, donant un aspecte groc als traços. Novament, una "
"altra bona raó per a treballar en RGB i permetre que la conversió sigui "
"realitzada per la impremta."

#: ../../general_concepts/colors/color_models.rst:130
msgid ""
"While CMYK has a smaller 'gamut' than RGB, however, it's still recommended "
"to use an RGB working space profile to do your editing in. Afterwards, you "
"can convert it to your printer's CMYK profile using either perceptual or "
"relative colorimetric intent. Or you can just give the workspace rgb image "
"to your printer and let them handle the work."
msgstr ""
"No obstant això, tot i que CMYK té una «gamma» més petita que RGB, encara es "
"recomana utilitzar un perfil de l'espai de treball RGB per a realitzar la "
"vostra edició. Després, podreu convertir-la al perfil CMYK de la vostra "
"impressora utilitzant el propòsit de la colorimetria perceptiva o relativa. "
"O simplement podreu donar-li la imatge en l'espai de treball RGB a la "
"impressora i deixar que aquesta s'encarregui de la feina."

#: ../../general_concepts/colors/color_models.rst:136
msgid "YCrCb"
msgstr "YCrCb"

#: ../../general_concepts/colors/color_models.rst:138
msgid "Luminosity, Red-chroma, Blue-chroma"
msgstr "Lluminositat, chroma del vermell i chroma del blau"

#: ../../general_concepts/colors/color_models.rst:140
msgid "YCrCb stands for:"
msgstr "YCrCb vol dir:"

#: ../../general_concepts/colors/color_models.rst:142
msgid "Y'/Y"
msgstr "Y'/Y"

#: ../../general_concepts/colors/color_models.rst:143
msgid "Luma/Luminosity, thus, the amount of light a color reflects."
msgstr ""
"Luma/Lluminositat, per tant, la quantitat de llum que reflecteix un color."

#: ../../general_concepts/colors/color_models.rst:144
msgid "Cr"
msgstr "Cr"

#: ../../general_concepts/colors/color_models.rst:145
msgid ""
"Red Chroma. This value measures how red a color is versus how green it is."
msgstr ""
"Chroma del vermell. Aquest valor mesura com de vermell té contra el verd que "
"és."

#: ../../general_concepts/colors/color_models.rst:147
msgid "Cb"
msgstr "Cb"

#: ../../general_concepts/colors/color_models.rst:147
msgid ""
"Blue Chroma. This value measures how blue a color is versus how yellow it is."
msgstr ""
"Chroma del blau. Aquest valor mesura com de blau té contra el groc que és."

#: ../../general_concepts/colors/color_models.rst:149
msgid ""
"This color space is often used in photography and in (correct) "
"implementations of JPEG. As a human you're much more sensitive to the "
"lightness of colors, and thus JPEG tries to compress the Cr and Cb channels, "
"and leave the Y channel in full quality."
msgstr ""
"Aquest espai de color s'utilitza sovint en la fotografia i en "
"implementacions (correctes) del JPEG. Com humà, sou molt més sensible a la "
"claredat dels colors i, per tant, el JPEG intenta comprimir els canals Cr i "
"Cb, i deixar el canal Y en qualitat total."

#: ../../general_concepts/colors/color_models.rst:153
msgid ""
"Krita doesn't bundle a ICC profile for YCrCb on the basis of there being no "
"open source ICC profiles for this color space. It's unusable without one, "
"and also probably very untested."
msgstr ""
"El Krita no inclou un perfil ICC per a YCrCb sobre la base que no hi ha "
"perfils ICC de codi obert per a aquest espai de color. Sense un és "
"inutilitzable, i probablement tampoc s'han fet proves."

#: ../../general_concepts/colors/color_models.rst:158
msgid "XYZ"
msgstr "XYZ"

#: ../../general_concepts/colors/color_models.rst:160
msgid ""
"Back in 1931, the CIE (Institute of Color and Light), was studying human "
"color perception. In doing so, they made the first color spaces, with XYZ "
"being the one best at approximating human vision."
msgstr ""
"El 1931, el CIE (Institut de color i llum), estava estudiant la percepció "
"del color pels humans. En fer-ho, van crear els primers espais de color, "
"sent el XYZ el millor per aproximar-se a la visió humana."

#: ../../general_concepts/colors/color_models.rst:163
msgid "It's almost impossible to really explain what XYZ is."
msgstr "És gairebé impossible explicar realment què és el XYZ."

#: ../../general_concepts/colors/color_models.rst:166
msgid "Is equal to green."
msgstr "És igual que el verd."

#: ../../general_concepts/colors/color_models.rst:167
msgid "Z"
msgstr "Z"

#: ../../general_concepts/colors/color_models.rst:168
msgid "Akin to blue."
msgstr "Similar al blau."

#: ../../general_concepts/colors/color_models.rst:170
msgid "X"
msgstr "X"

#: ../../general_concepts/colors/color_models.rst:170
msgid "Is supposed to be red."
msgstr "Se suposa que és vermell."

#: ../../general_concepts/colors/color_models.rst:172
msgid ""
"XYZ is used as a baseline reference for all other profiles and models. All "
"color conversions are done in XYZ, and all profiles coordinates match XYZ."
msgstr ""
"El XYZ s'utilitza com a referència de la línia base per a tots els altres "
"perfils i models. Totes les conversions de color es realitzen en el XYZ, i "
"totes les coordenades dels perfils coincideixen amb el XYZ."

#: ../../general_concepts/colors/color_models.rst:177
msgid "L\\*a\\*b\\*"
msgstr "L\\*a\\*b\\*"

#: ../../general_concepts/colors/color_models.rst:179
msgid "Stands for:"
msgstr "Vol dir:"

#: ../../general_concepts/colors/color_models.rst:181
msgid "L\\*"
msgstr "L\\*"

#: ../../general_concepts/colors/color_models.rst:182
msgid "Lightness, similar to luminosity in this case."
msgstr "Claredat, en aquest cas és similar a la lluminositat."

#: ../../general_concepts/colors/color_models.rst:183
msgid "a\\*"
msgstr "a\\*"

#: ../../general_concepts/colors/color_models.rst:184
msgid ""
"a\\* in this case is the measurement of how magenta a color is versus how "
"green it is."
msgstr ""
"a\\* en aquest cas és la mesura com de magenta té un color contra el verd "
"que és."

#: ../../general_concepts/colors/color_models.rst:186
msgid "b\\*"
msgstr "b\\*"

#: ../../general_concepts/colors/color_models.rst:186
msgid ""
"b\\* in this case is a measurement of how yellow a color is versus how blue "
"a color is."
msgstr ""
"b\\* en aquest cas és la mesura com de groc té un color contra el blau que "
"és."

#: ../../general_concepts/colors/color_models.rst:188
msgid ""
"L\\*a\\*b\\* is supposed to be a more comprehensible variety of XYZ and the "
"most 'complete' of all color spaces. It's often used as an in between color "
"space in conversion, but even more as the correct color space to do color-"
"balancing in. It's far easier to adjust the contrast and color tone in "
"L*a*b*."
msgstr ""
"Se suposa que L\\*a\\*b\\* és una varietat més comprensible del XYZ i el més "
"«complet» de tots els espais de color. Sovint s'utilitza com un espai de "
"color intermedi durant la conversió, però encara més com a l'espai de color "
"correcte per a realitzar l'equilibri de color. És molt més fàcil ajustar el "
"contrast i el to del color en el L*a*b*."

#: ../../general_concepts/colors/color_models.rst:190
msgid ""
"L\\*a\\*b\\* is technically the same as Photoshop's LAB. Photoshop "
"specifically uses CIELAB d50."
msgstr ""
"El L*a*b* és tècnicament el mateix que el LAB del Photoshop. El Photoshop "
"utilitza específicament CIELAB d50."

#: ../../general_concepts/colors/color_models.rst:193
msgid "Filters and blending modes"
msgstr "Filtres i modes de barreja"

#: ../../general_concepts/colors/color_models.rst:195
msgid ""
"Maybe you have noticed that blending modes in LAB don't work like they do in "
"RGB or CMYK. This is because the blending modes work by doing a bit of maths "
"on the color coordinates, and because color coordinates are different per "
"color space, the blending modes look different."
msgstr ""
"Potser heu notat que els modes de barreja en el LAB no funcionen com ho fan "
"en el RGB o CMYK. Això es deu al fet que els modes de barreja treballen fent "
"una mica de matemàtiques sobre les coordenades del color, i perquè les "
"coordenades del color són diferents segons l'espai de color, els modes de "
"barreja es veuran diferent."
