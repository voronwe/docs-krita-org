# Translation of docs_krita_org_reference_manual___layers_and_masks___filter_layers.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-06-02 17:21+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.1\n"

#: ../../reference_manual/layers_and_masks/filter_layers.rst:1
msgid "How to use filter layers in Krita."
msgstr "Com emprar les capes de filtratge en el Krita."

#: ../../reference_manual/layers_and_masks/filter_layers.rst:14
msgid "Layers"
msgstr "Capes"

#: ../../reference_manual/layers_and_masks/filter_layers.rst:14
msgid "Filters"
msgstr "Filtres"

#: ../../reference_manual/layers_and_masks/filter_layers.rst:19
msgid "Filter Layer"
msgstr "Capa de filtratge"

#: ../../reference_manual/layers_and_masks/filter_layers.rst:22
msgid ""
"Filter layers show whatever layers are underneath them, but with a filter "
"such as Layer Styles, Blur, Levels, Brightness / Contrast. For example, if "
"you add a **Filter Layer**, and choose the Blur filter, you will see every "
"layer under your filter layer blurred."
msgstr ""
"Les capes de filtratge mostren les capes que es troben sota seu, però amb un "
"filtre com Estils de la capa, Difuminat, Nivells, Brillantor / Contrast. Per "
"exemple, si afegiu una **Capa de filtratge** i trieu el Filtre de difuminat, "
"veureu borroses cada capa sota la capa de filtratge."

#: ../../reference_manual/layers_and_masks/filter_layers.rst:24
msgid ""
"Unlike applying a filter directly on to a section of a Paint Layer, Filter "
"Layers do not actually alter the original image in the Paint Layers below "
"them. Once again, non-destructive editing! You can tweak the filter at any "
"time, and the changes can always be altered or removed."
msgstr ""
"A diferència d'aplicar un filtre directament sobre una secció d'una Capa de "
"pintura, les Capes de filtratge en realitat no alteraran la imatge original "
"a les Capes de pintura que hi ha sota seu. Un vegada més, edició no "
"destructiva! Podreu modificar el filtre en qualsevol moment i els canvis "
"sempre es podran alterar o eliminar."

#: ../../reference_manual/layers_and_masks/filter_layers.rst:26
msgid ""
"Unlike Filter Masks though, Filter Layers apply to the entire canvas for the "
"layers beneath. If you wish to apply a filter layer to only *some* layers, "
"then you can utilize the Group Layer feature and add those layers into a "
"group with the filter layer on top of the stack."
msgstr ""
"No obstant això, a diferència de les Màscares de filtratge, les Capes de "
"filtratge s'apliquen a tot el llenç per a les capes que es troben a sota. "
"Per aplicar una capa de filtratge només en *algunes* capes, podreu utilitzar "
"la característica Capa de grup i afegir aquestes capes a un grup amb la capa "
"de filtratge a la part superior de la pila."

#: ../../reference_manual/layers_and_masks/filter_layers.rst:28
msgid ""
"You can edit the settings for a filter layer, by double clicking on it in "
"the Layers docker."
msgstr ""
"Podreu editar els ajustaments per a una capa de filtratge, fent doble clic "
"sobre seu a l'acoblador Capes."

#: ../../reference_manual/layers_and_masks/filter_layers.rst:31
msgid ""
"Only Krita native filters (the ones in the :guilabel:`Filters` menu) can be "
"used with Filter Layers. Filter Layers are not supported using the "
"externally integrated G'Mic filters."
msgstr ""
"Només els filtres nadius del Krita (els que estan al menú :guilabel:"
"`Filtres`) es poden utilitzar amb les Capes de filtratge. Les Capes de "
"filtratge no són compatibles amb els filtres del G'Mic integrats externament."
