# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-07-19 11:41+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-POFile-SpellExtra: Krita ref gbr GBR\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-IgnoreConsistency: Name\n"

#: ../../general_concepts/file_formats/file_gbr.rst:1
msgid "The Gimp Brush file format as used in Krita."
msgstr "O formato de ficheiro de Pincéis do Gimp, tal como é usado no Krita."

#: ../../general_concepts/file_formats/file_gbr.rst:10
msgid "Gimp Brush"
msgstr "Pincel do Gimp"

#: ../../general_concepts/file_formats/file_gbr.rst:10
msgid "GBR"
msgstr "GBR"

#: ../../general_concepts/file_formats/file_gbr.rst:10
msgid "*.gbr"
msgstr "*.gbr"

#: ../../general_concepts/file_formats/file_gbr.rst:15
msgid "\\*.gbr"
msgstr "\\*.gbr"

#: ../../general_concepts/file_formats/file_gbr.rst:17
msgid ""
"The GIMP brush format. Krita can open, save and use these files as :ref:"
"`predefined brushes <predefined_brush_tip>`."
msgstr ""
"O formato de pincéis do GIMP. O Krita consegue abrir, gravar e usar estes "
"ficheiros como :ref:`pincéis predefinidos <predefined_brush_tip>`."

#: ../../general_concepts/file_formats/file_gbr.rst:19
msgid ""
"There's three things that you can decide upon when exporting a ``*.gbr``:"
msgstr "Existem três coisas que pode decidir quando exportar um ``*.gbr``:"

#: ../../general_concepts/file_formats/file_gbr.rst:21
msgid "Name"
msgstr "Nome"

#: ../../general_concepts/file_formats/file_gbr.rst:22
msgid ""
"This name is different from the file name, and will be shown inside Krita as "
"the name of the brush."
msgstr ""
"Este nome é diferente do do ficheiro, e será apresentado dentro do Krita "
"como sendo o nome do pincel."

#: ../../general_concepts/file_formats/file_gbr.rst:23
msgid "Spacing"
msgstr "Espaço"

#: ../../general_concepts/file_formats/file_gbr.rst:24
msgid "This sets the default spacing."
msgstr "Isto define o espaço ou intervalo predefinido."

#: ../../general_concepts/file_formats/file_gbr.rst:26
msgid "Use color as mask"
msgstr "Usar a cor como máscara"

#: ../../general_concepts/file_formats/file_gbr.rst:26
msgid ""
"This'll turn the darkest values of the image as the ones that paint, and the "
"whitest as transparent. Untick this if you are using colored images for the "
"brush."
msgstr ""
"Isto irá transformar os valores mais escuros da imagem nos que pintam e os "
"mais claros em transparentes. Desligue isto se estiver a usar imagens "
"coloridas para o pincel."

#: ../../general_concepts/file_formats/file_gbr.rst:28
msgid ""
"GBR brushes are otherwise unremarkable, and limited to 8bit color precision."
msgstr ""
"Os pincéis GBR não têm a possibilidade de serem marcados de novo e estão "
"limitados a uma precisão de 8 bits na cor."
