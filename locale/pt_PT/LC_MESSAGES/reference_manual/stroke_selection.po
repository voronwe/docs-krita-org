# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-17 15:01+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: en Krita image menuselection Kritastrokeselection\n"
"X-POFile-SpellExtra: Strokeselection images StrokeSelection\n"

#: ../../<rst_epilog>:1
msgid ".. image:: images/Stroke_Selection_4.png"
msgstr ".. image:: images/Stroke_Selection_4.png"

#: ../../reference_manual/stroke_selection.rst:1
msgid "How to use the stroke selection command in Krita."
msgstr "Como usar o comando para traçar a selecção no Krita."

#: ../../reference_manual/stroke_selection.rst:10
msgid "Selection"
msgstr "Selecção"

#: ../../reference_manual/stroke_selection.rst:10
msgid "Stroke"
msgstr "Traço"

#: ../../reference_manual/stroke_selection.rst:15
msgid "Stroke Selection"
msgstr "Traçar a Selecção"

#: ../../reference_manual/stroke_selection.rst:17
msgid ""
"Sometimes, you want to add an even border around a selection. Stroke "
"Selection allows you to do this. It's under :menuselection:`Edit --> Stroke "
"Selection`."
msgstr ""
"Em alguns casos, deseja adicionar um contorno consistente em torno de uma "
"dada selecção. A opção Traçar a Selecção permite-lhe fazer isso. Encontra-se "
"em :menuselection:`Editar --> Traçar a Selecção`."

#: ../../reference_manual/stroke_selection.rst:19
msgid "First make a selection and call up the menu:"
msgstr "Primeiro faça uma selecção e invoque o menu:"

#: ../../reference_manual/stroke_selection.rst:22
msgid ".. image:: images/Krita_stroke_selection_1.png"
msgstr ".. image:: images/Krita_stroke_selection_1.png"

#: ../../reference_manual/stroke_selection.rst:23
msgid ""
"The main options are about using the current brush, or lining the selection "
"with an even line. You can use the current foreground color, the background "
"color or a custom color."
msgstr ""
"As opções principais centram-se em torno do pincel actual ou do alinhamento "
"da selecção com uma linha recta. Poderá usar a cor principal actual, a cor "
"de fundo ou uma cor personalizada."

#: ../../reference_manual/stroke_selection.rst:25
msgid "Using the current brush allows you to use textured brushes:"
msgstr "O uso do pincel actual permite-lhe usar pincéis com texturas:"

#: ../../reference_manual/stroke_selection.rst:28
msgid ".. image:: images/Stroke_selection_2.png"
msgstr ".. image:: images/Stroke_selection_2.png"

#: ../../reference_manual/stroke_selection.rst:29
msgid ""
"Lining the selection also allows you to set the background color, on top of "
"the line width in pixels or inches:"
msgstr ""
"O alinhamento da selecção também lhe permite definir a cor de fundo, sobre a "
"espessura da linha, em pixels ou polegadas:"

#: ../../reference_manual/stroke_selection.rst:32
msgid ".. image:: images/Krita_stroke_selection_3.png"
msgstr ".. image:: images/Krita_stroke_selection_3.png"

#: ../../reference_manual/stroke_selection.rst:33
msgid "This creates nice silhouettes:"
msgstr "Isto cria algumas silhuetas interessantes:"
