# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-17 14:52+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: en Krita transfer image color Color images alpha\n"
"X-POFile-SpellExtra: filters\n"

#: ../../reference_manual/filters/colors.rst:None
msgid ".. image:: images/filters/Krita-color-to-alpha.png"
msgstr ".. image:: images/filters/Krita-color-to-alpha.png"

#: ../../reference_manual/filters/colors.rst:1
msgid "Overview of the color filters."
msgstr "Introdução aos filtros de cores."

#: ../../reference_manual/filters/colors.rst:11
msgid "Filters"
msgstr "Filtros"

#: ../../reference_manual/filters/colors.rst:16
msgid "Color"
msgstr "Cor"

#: ../../reference_manual/filters/colors.rst:18
msgid ""
"Similar to the Adjust filters, the color filters are image wide color "
"operations."
msgstr ""
"Semelhante aos filtros de Ajuste, os filtros de cores são operações de cores "
"amplas da imagem."

#: ../../reference_manual/filters/colors.rst:20
#: ../../reference_manual/filters/colors.rst:24
msgid "Color to Alpha"
msgstr "Cor para o 'Alfa'"

#: ../../reference_manual/filters/colors.rst:26
msgid ""
"This filter allows you to make one single color transparent (alpha). By "
"default when you run this filter white is selected, you can choose a color "
"that you want to make transparent from the color selector."
msgstr ""
"Este filtro permite-lhe tornar uma única cor transparente ('alfa'). Por "
"omissão, quando executar este filtro, estará seleccionado o branco, mas "
"poderá escolher uma outra cor que deseje tornar transparente no selector de "
"cores."

#: ../../reference_manual/filters/colors.rst:29
msgid ".. image:: images/filters/Color-to-alpha.png"
msgstr ".. image:: images/filters/Color-to-alpha.png"

#: ../../reference_manual/filters/colors.rst:30
msgid ""
"The Threshold indicates how much other colors will be considered mixture of "
"the removed color and non-removed colors. For example, with threshold set to "
"255, and the removed color set to white, a 50% gray will be considered a "
"mixture of black+white, and thus transformed in a 50% transparent black."
msgstr ""
"O Limiar indica qual a proporção das outras cores que serão consideradas "
"como uma mistura da cor removida com as cores não-removidas. Por exemplo, "
"com o limiar igual a 255 e a cor removida como sendo a branca, um cinzento a "
"50% será considerado uma mistura de preto e branco e, como tal, será "
"transformado num preto 50% transparente."

#: ../../reference_manual/filters/colors.rst:36
msgid ""
"This filter is really useful in separating line art from the white "
"background."
msgstr "Este filtro é realmente útil na separação das linhas do fundo branco."

#: ../../reference_manual/filters/colors.rst:41
msgid "Color Transfer"
msgstr "Transferência de Cores"

#: ../../reference_manual/filters/colors.rst:43
msgid ""
"This filter converts the colors of the image to colors from the reference "
"image. This is a quick way to change a color combination of an artwork to an "
"already saved image or a reference image."
msgstr ""
"Este filtro converte as cores da imagem para as cores da imagem de "
"referência. Esta é uma forma rápida de mudar uma combinação de cores de uma "
"obra para uma imagem já gravada ou uma imagem de referência."

#: ../../reference_manual/filters/colors.rst:47
msgid ".. image:: images/filters/Color-transfer.png"
msgstr ".. image:: images/filters/Color-transfer.png"

#: ../../reference_manual/filters/colors.rst:51
msgid "Maximize Channel"
msgstr "Maximizar o Canal"

#: ../../reference_manual/filters/colors.rst:53
msgid ""
"This filter checks for all the channels of a each single color and set all "
"but the highest value to 0."
msgstr ""
"Este filtro verifica todos os canais de uma dada cor única e configura todos "
"os valores menos o máximo como 0."

#: ../../reference_manual/filters/colors.rst:58
msgid "Minimize Channel"
msgstr "Minimizar o Canal"

#: ../../reference_manual/filters/colors.rst:60
msgid ""
"This is reverse to Maximize channel, it checks all the channels of a each "
"single color and sets all but the lowest to 0."
msgstr ""
"Isto é um inverso do Maximizar o Canal, onde verifica todos os canais de uma "
"dada cor única e configura todos menos o mínimo como 0."
