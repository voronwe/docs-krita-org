# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-03 14:08+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: Movetoolcoordinates en toolmove icons Krita image\n"
"X-POFile-SpellExtra: movetool kbd images alt tools\n"

#: ../../<generated>:1
msgid "Position"
msgstr "Posição"

#: ../../<rst_epilog>:44
msgid ""
".. image:: images/icons/move_tool.svg\n"
"   :alt: toolmove"
msgstr ""
".. image:: images/icons/move_tool.svg\n"
"   :alt: ferramenta para mover"

#: ../../reference_manual/tools/move.rst:0
msgid ".. image:: images/tools/Movetool_coordinates.png"
msgstr ".. image:: images/tools/Movetool_coordinates.png"

#: ../../reference_manual/tools/move.rst:1
msgid "Krita's move tool reference."
msgstr "A referência da ferramenta de movimento do Krita."

#: ../../reference_manual/tools/move.rst:11
msgid "Tools"
msgstr "Ferramentas"

#: ../../reference_manual/tools/move.rst:11
msgid "Move"
msgstr "Mover"

#: ../../reference_manual/tools/move.rst:11
msgid "Transform"
msgstr "Transformar"

#: ../../reference_manual/tools/move.rst:16
msgid "Move Tool"
msgstr "Ferramenta de Movimento"

#: ../../reference_manual/tools/move.rst:18
msgid "|toolmove|"
msgstr "|toolmove|"

#: ../../reference_manual/tools/move.rst:20
msgid ""
"With this tool, you can move the current layer or selection by dragging the "
"mouse."
msgstr ""
"Com esta ferramenta, poderá mover a camada ou selecção actual através do "
"arrastamento do rato."

#: ../../reference_manual/tools/move.rst:22
msgid "Move current layer"
msgstr "Mover a camada actual"

#: ../../reference_manual/tools/move.rst:23
msgid "Anything that is on the selected layer will be moved."
msgstr "Tudo o que estiver na camada seleccionada será movido."

#: ../../reference_manual/tools/move.rst:24
msgid "Move layer with content"
msgstr "Mover a camada com o conteúdo"

#: ../../reference_manual/tools/move.rst:25
msgid ""
"Any content contained on the layer that is resting under the four-headed "
"Move cursor will be moved."
msgstr ""
"Todo o conteúdo existente na camada que está sob o cursor de quatro pontos "
"para Mover será movido de facto."

#: ../../reference_manual/tools/move.rst:26
msgid "Move the whole group"
msgstr "Mover o grupo inteiro"

#: ../../reference_manual/tools/move.rst:27
msgid ""
"All content on all layers will move.  Depending on the number of layers this "
"might result in slow and, sometimes, jerky movements. Use this option "
"sparingly or only when necessary."
msgstr ""
"Todo o conteúdo de todas as camadas será movido. Dependendo do número de "
"camadas, isto poderá originar alguns movimentos lentos e com perturbações. "
"Use esta opção raramente ou só mesmo quando for necessário."

#: ../../reference_manual/tools/move.rst:28
msgid "Shortcut move distance (3.0+)"
msgstr "Distância de movimento dos atalhos (3.0+)"

#: ../../reference_manual/tools/move.rst:29
msgid ""
"This allows you to set how much, and in which units, the :kbd:`Left Arrow`, :"
"kbd:`Up Arrow`, :kbd:`Right Arrow` and :kbd:`Down Arrow` cursor key actions "
"will move the layer."
msgstr ""
"Isto permite-lhe definir quanto, e com que unidades, as acções das teclas :"
"kbd:`Esquerda`, :kbd:`Cima`, :kbd:`Direita` e :kbd:`Baixo` irão mover a "
"camada."

#: ../../reference_manual/tools/move.rst:30
msgid "Large Move Scale (3.0+)"
msgstr "Escala de Movimentos Grandes (3.0+)"

#: ../../reference_manual/tools/move.rst:31
msgid ""
"Allows you to multiply the movement of the Shortcut Move Distance when "
"pressing the :kbd:`Shift` key before pressing a direction key."
msgstr ""
"Permite-lhe multiplicar o movimento da Distância de Movimento dos Atalhos, "
"se carregar em :kbd:`Shift` antes de carregar numa tecla de cursores."

#: ../../reference_manual/tools/move.rst:32
msgid "Show coordinates"
msgstr "Mostrar as coordenadas"

#: ../../reference_manual/tools/move.rst:33
msgid ""
"When toggled will show the coordinates of the top-left pixel of the moved "
"layer in a floating window."
msgstr ""
"Quando mudar de estado, irá mostrar as coordenadas no pixel no canto "
"superior esquerdo da camada movida numa janela flutuante."

#: ../../reference_manual/tools/move.rst:35
msgid ""
"If you click, then press the :kbd:`Shift` key, then move the layer, movement "
"is constrained to the horizontal and vertical directions. If you press the :"
"kbd:`Shift` key, then click, then move, all layers will be moved, with the "
"movement constrained to the horizontal and vertical directions"
msgstr ""
"Se carregar, então carregue em :kbd:`Shift`, depois mova a camada, sendo que "
"o movimento ficará restrito apenas às direcções horizontais e verticais. Se "
"carregar em :kbd:`Shift`, depois com o rato, depois movendo, então todas as "
"camadas serão movidas com o movimento restrito apenas às direcções "
"horizontal e vertical"

#: ../../reference_manual/tools/move.rst:37
msgid "Constrained movement"
msgstr "Movimento restrito"

#: ../../reference_manual/tools/move.rst:40
msgid ""
"Gives the top-left coordinate of the layer, can also be manually edited."
msgstr ""
"Dá a coordenada superior esquerda da camada, sendo que pode ser editada de "
"forma manual."
