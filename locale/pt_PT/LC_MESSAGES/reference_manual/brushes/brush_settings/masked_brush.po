# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-09-01 03:12+0200\n"
"PO-Revision-Date: 2019-06-17 15:01+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: optionsize en optionopacitynflow optionrotation image\n"
"X-POFile-SpellExtra: optionbrushtip jpg brush Masking images ref\n"
"X-POFile-SpellExtra: optionmirror optiontexture Photoshop Krita demão\n"
"X-POFile-SpellExtra: optionratio pixelbrushengine optionscatter brushes\n"

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:0
msgid ".. image:: images/brushes/Masking-brush2.jpg"
msgstr ".. image:: images/brushes/Masking-brush2.jpg"

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:None
msgid ".. image:: images/brushes/Masking-brush1.jpg"
msgstr ".. image:: images/brushes/Masking-brush1.jpg"

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:1
msgid ""
"How to use the masked brush functionality in Krita. This functionality is "
"not unlike the dual brush option from photoshop."
msgstr ""
"Como usar a funcionalidade do pincel com máscara no Krita. Esta "
"funcionalidade não é muito diferente da opção de pincel duplo do Photoshop."

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:13
#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:18
msgid "Masked Brush"
msgstr "Pincel Mascarado"

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:13
msgid "Dual Brush"
msgstr "Pincel Duplo"

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:13
msgid "Stacked Brush"
msgstr "Pincel Empilhado"

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:23
msgid ""
"Masked brush is new feature that is only available in the :ref:"
"`pixel_brush_engine`. They are additional settings you will see in the brush "
"editor. Masked brushes allow you to combine two brush tips in one stroke. "
"One brush tip will be a mask for your primary brush tip. A masked brush is a "
"good alternative to texture for creating expressive and textured brushes."
msgstr ""
"O pincel mascarado ou com máscara é uma nova funcionalidade que só está "
"disponível no :ref:`pixel_brush_engine`. São opções adicionais que poderá "
"ver no editor do pincel. Os pincéis com máscara permitem-lhe combinar duas "
"pontas de pincel num único traço. Uma das pontas será uma máscara para a sua "
"ponta principal. Um pincel com máscara é uma boa alternativa a uma textura "
"para criar pincéis expressivos e com texturas."

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:29
msgid ""
"Due to technological constraints, the masked brush only works in the wash "
"painting mode. However, do remember that flow works as opacity does in the "
"build-up painting mode."
msgstr ""
"devido a restrições tecnológicas, o pincel com máscara só funciona no modo "
"de pintura por lavagem. Contudo, lembre-se que o fluxo funciona como a "
"opacidade no modo de pintura por composição."

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:32
msgid ""
"Like with normal brush tip you can choose any brush tip and change it size, "
"spacing, and rotation. Masking brush size is relative to main brush size. "
"This means when you change your brush size masking tip will be changed to "
"keep the ratio."
msgstr ""
"Como nas pontas de pincéis normais, poderá escolher qualquer ponta e mudar o "
"seu tamanho, intervalo e rotação. O tamanho do pincel com máscara é relativo "
"ao tamanho do pincel principal. Isto significa que, quando mudar o tamanho "
"da ponta, a ponta da máscara será também mudada para manter as proporções."

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:35
msgid ":ref:`Blending mode (drop-down inside Brush tip)<blending_modes>`:"
msgstr ""
":ref:`Modo de mistura (lista dentro da Ponta do Pincel)<blending_modes>`:"

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:35
msgid "Blending modes changes how tips are combined."
msgstr "Os modos de mistura alteram como se combinam as pontas."

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:38
msgid ":ref:`option_brush_tip`"
msgstr ":ref:`option_brush_tip`"

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:40
msgid ":ref:`option_size`"
msgstr ":ref:`option_size`"

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:41
msgid "The size sensor option of the second tip."
msgstr "A opção do sensor do tamanho da segunda ponta."

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:42
msgid ":ref:`option_opacity_n_flow`"
msgstr ":ref:`option_opacity_n_flow`"

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:43
msgid ""
"The opacity and flow of the second tip. This is mapped to a sensor by "
"default. Flow can be quite aggressive on subtract mode, so it might be an "
"idea to turn it off there."
msgstr ""
"A opacidade e o fluxo da segunda ponta. Isto está associado por omissão a um "
"sensor. O fluxo pode ser bastante agressivo no modo subtractivo, pelo que "
"poderá ser uma boa ideia desligá-lo aí."

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:44
msgid ":ref:`option_ratio`"
msgstr ":ref:`option_ratio`"

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:45
msgid "This affects the brush ratio on a given brush."
msgstr "Isto afecta as proporções de tamanho do pincel para um dado pincel."

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:46
msgid ":ref:`option_mirror`"
msgstr ":ref:`option_mirror`"

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:47
msgid "The Mirror option of the second tip."
msgstr "A opção de Espelho da segunda ponta."

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:48
msgid ":ref:`option_rotation`"
msgstr ":ref:`option_rotation`"

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:49
msgid "The rotation option of the second tip. Best set to \"fuzzy dab\"."
msgstr ""
"A opção de rotação da segunda ponta. Melhor se ficar configurado como "
"\"demão difusa\"."

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:51
msgid ":ref:`option_scatter`"
msgstr ":ref:`option_scatter`"

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:51
msgid ""
"The scatter option. The default is quite high, so don't forget to turn it "
"lower."
msgstr ""
"A opção de dispersão. Por omissão, é muito elevada, por isso não se esqueça "
"de baixar o valor dela."

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:53
msgid "Difference from :ref:`option_texture`:"
msgstr "Diferença do :ref:`option_texture`:"

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:55
msgid "You don’t need seamless texture to make cool looking brush"
msgstr ""
"Não precisa de uma textura transparente para criar um pincel com bom aspecto"

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:56
msgid "Stroke generates on the fly, it always different"
msgstr "O traço é gerado na hora, portanto é sempre diferente"

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:57
msgid "Brush strokes looks same on any brush size"
msgstr "Os traços parecem iguais com qualquer tamanho de pincel"

#: ../../reference_manual/brushes/brush_settings/masked_brush.rst:58
msgid ""
"Easier to fill some areas with solid color but harder to make it hard "
"textured"
msgstr ""
"É mais fácil preencher algumas áreas com cores únicas, mas mais difícil de "
"as preencher com texturas fortes"
