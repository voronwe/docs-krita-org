# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-15 08:26+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../reference_manual/blending_modes.rst:1
msgid "Overview of Krita's blending modes."
msgstr "Översikt av Kritas blandningslägen."

#: ../../reference_manual/blending_modes.rst:10
msgid "Blending Modes!"
msgstr "Blandningslägen!"

#: ../../reference_manual/blending_modes.rst:16
msgid "Blending Modes"
msgstr "Blandningslägen"

#: ../../reference_manual/blending_modes.rst:18
msgid ""
"Blending modes are a little difficult to explain. Basically, when one layer "
"is above the other, the computer uses a bit of programming to decide how the "
"combination of both layers will look."
msgstr ""
"Blandningslägen är lite svåra att förklara. I grund och botten, när ett "
"lager är ovanpå ett annat, använder datorn en del programmering för att "
"bestämma hur kombinationen av de båda lagren ska se ut."

#: ../../reference_manual/blending_modes.rst:20
msgid ""
"Blending modes can not just apply to Layers, but also to individual strokes."
msgstr ""
"Blandningslägen kan inte bara gälla lager, utan också individuella drag."

#: ../../reference_manual/blending_modes.rst:23
msgid "Favorites"
msgstr "Favoriter"

#: ../../reference_manual/blending_modes.rst:25
msgid ""
"These are the blending modes that have been ticked as favorites, defaulting "
"these are:"
msgstr ""
"De följande är blandningslägen som har markerats som favoriter, "
"standardmässigt är de:"

#: ../../reference_manual/blending_modes.rst:27
msgid ":ref:`bm_addition`"
msgstr ":ref:`bm_addition`"

#: ../../reference_manual/blending_modes.rst:28
msgid ":ref:`bm_color_burn`"
msgstr ":ref:`bm_color_burn`"

#: ../../reference_manual/blending_modes.rst:29
msgid ":ref:`bm_color`"
msgstr ":ref:`bm_color`"

#: ../../reference_manual/blending_modes.rst:30
msgid ":ref:`bm_color_dodge`"
msgstr ":ref:`bm_color_dodge`"

#: ../../reference_manual/blending_modes.rst:31
msgid ":ref:`bm_darken`"
msgstr ":ref:`bm_darken`"

#: ../../reference_manual/blending_modes.rst:32
msgid ":ref:`bm_erase`"
msgstr ":ref:`bm_erase`"

#: ../../reference_manual/blending_modes.rst:33
msgid ":ref:`bm_lighten`"
msgstr ":ref:`bm_lighten`"

#: ../../reference_manual/blending_modes.rst:34
msgid ":ref:`bm_luminosity`"
msgstr ":ref:`bm_luminosity`"

#: ../../reference_manual/blending_modes.rst:35
msgid ":ref:`bm_multiply`"
msgstr ":ref:`bm_multiply`"

#: ../../reference_manual/blending_modes.rst:36
msgid ":ref:`bm_normal`"
msgstr ":ref:`bm_normal`"

#: ../../reference_manual/blending_modes.rst:37
msgid ":ref:`bm_overlay`"
msgstr ":ref:`bm_overlay`"

#: ../../reference_manual/blending_modes.rst:38
msgid ":ref:`bm_saturation`"
msgstr ":ref:`bm_saturation`"

#: ../../reference_manual/blending_modes.rst:41
msgid "Hotkeys associated with Blending modes"
msgstr "Snabbtangenter som hör ihop med blandningslägen"

#: ../../reference_manual/blending_modes.rst:43
msgid ""
"Defaultly the following hotkeys are associated with blending modes used for "
"painting. Note: these shortcuts do not change the blending mode of the "
"current layer."
msgstr ""
"Standardmässigt är följande snabbtangenter associerade med blandningslägen "
"som används för målning. Observera: genvägarna ändrar inte blandningsläget "
"för aktuellt lager."

#: ../../reference_manual/blending_modes.rst:45
msgid ""
"You first need to use modifiers :kbd:`Alt + Shift`, then use the following "
"hotkey to have the associated blending mode:"
msgstr ""
"Först måste väljarna :kbd:`Alt + Skift` användas, och sedan följande "
"snabbtangent för att få motsvarande blandningsläge:"

#: ../../reference_manual/blending_modes.rst:48
msgid ":kbd:`A` :ref:`bm_linear_burn`"
msgstr ":kbd:`A` :ref:`bm_linear_burn`"

#: ../../reference_manual/blending_modes.rst:49
msgid ":kbd:`B` :ref:`bm_color_burn`"
msgstr ":kbd:`B` :ref:`bm_color_burn`"

#: ../../reference_manual/blending_modes.rst:50
msgid ":kbd:`C` :ref:`bm_color`"
msgstr ":kbd:`C` :ref:`bm_color`"

#: ../../reference_manual/blending_modes.rst:51
msgid ":kbd:`D` :ref:`bm_color_dodge`"
msgstr ":kbd:`D` :ref:`bm_color_dodge`"

#: ../../reference_manual/blending_modes.rst:52
msgid ":kbd:`E` :ref:`bm_difference`"
msgstr ":kbd:`E` :ref:`bm_difference`"

#: ../../reference_manual/blending_modes.rst:53
msgid ":kbd:`F` :ref:`bm_soft_light`"
msgstr ":kbd:`F` :ref:`bm_soft_light`"

#: ../../reference_manual/blending_modes.rst:54
msgid ":kbd:`I` :ref:`bm_dissolve`"
msgstr ":kbd:`I` :ref:`bm_dissolve`"

#: ../../reference_manual/blending_modes.rst:55
msgid ":kbd:`J` :ref:`bm_linear_light`"
msgstr ":kbd:`J` :ref:`bm_linear_light`"

#: ../../reference_manual/blending_modes.rst:56
msgid ":kbd:`K` :ref:`bm_darken`"
msgstr ":kbd:`K` :ref:`bm_darken`"

#: ../../reference_manual/blending_modes.rst:57
msgid ":kbd:`L` :ref:`bm_hard_mix`"
msgstr ":kbd:`L` :ref:`bm_hard_mix`"

#: ../../reference_manual/blending_modes.rst:58
msgid ":kbd:`M` :ref:`bm_multiply`"
msgstr ":kbd:`M` :ref:`bm_multiply`"

#: ../../reference_manual/blending_modes.rst:59
msgid ":kbd:`O` :ref:`bm_overlay`"
msgstr ":kbd:`O` :ref:`bm_overlay`"

#: ../../reference_manual/blending_modes.rst:60
msgid ":kbd:`Q` :ref:`bm_behind`"
msgstr ":kbd:`Q` :ref:`bm_behind`"

#: ../../reference_manual/blending_modes.rst:61
msgid ":kbd:`R` :ref:`bm_normal`"
msgstr ":kbd:`R` :ref:`bm_normal`"

#: ../../reference_manual/blending_modes.rst:62
msgid ":kbd:`S` :ref:`bm_screen`"
msgstr ":kbd:`S` :ref:`bm_screen`"

#: ../../reference_manual/blending_modes.rst:63
msgid ":kbd:`T` :ref:`bm_saturation`"
msgstr ":kbd:`T` :ref:`bm_saturation`"

#: ../../reference_manual/blending_modes.rst:64
msgid ":kbd:`U` :ref:`bm_hue`"
msgstr ":kbd:`U` :ref:`bm_hue`"

#: ../../reference_manual/blending_modes.rst:65
msgid ":kbd:`V` :ref:`bm_vivid_light`"
msgstr ":kbd:`V` :ref:`bm_vivid_light`"

#: ../../reference_manual/blending_modes.rst:66
msgid ":kbd:`W` :ref:`bm_exclusion`"
msgstr ":kbd:`W` :ref:`bm_exclusion`"

#: ../../reference_manual/blending_modes.rst:67
msgid ":kbd:`X` :ref:`bm_linear_dodge`"
msgstr ":kbd:`X` :ref:`bm_linear_dodge`"

#: ../../reference_manual/blending_modes.rst:68
msgid ":kbd:`Y` :ref:`bm_luminosity`"
msgstr ":kbd:`Y` :ref:`bm_luminosity`"

#: ../../reference_manual/blending_modes.rst:69
msgid ":kbd:`Z` :ref:`bm_pin_light`"
msgstr ":kbd:`Z` :ref:`bm_pin_light`"

#: ../../reference_manual/blending_modes.rst:70
msgid "Next Blending Mode :kbd:`+`"
msgstr "Nästa blandningsläge :kbd:`+`"

#: ../../reference_manual/blending_modes.rst:71
msgid "Previous Blending Mode :kbd:`-`"
msgstr "Föregående blandningsläge :kbd:`-`"

#: ../../reference_manual/blending_modes.rst:74
msgid "Available Blending Modes"
msgstr "Tillgängliga blandningsläge"

#: ../../reference_manual/blending_modes.rst:84
msgid "Basic blending modes:"
msgstr "Grundläggande blandningslägen:"

#: ../../reference_manual/blending_modes.rst:85
msgid "https://en.wikipedia.org/wiki/Blend_modes"
msgstr "https://en.wikipedia.org/wiki/Blend_modes"

#: ../../reference_manual/blending_modes.rst:86
msgid "Grain Extract/Grain Merge:"
msgstr "Extrahera och sammanfoga ådring:"

#: ../../reference_manual/blending_modes.rst:87
msgid "https://docs.gimp.org/en/gimp-concepts-layer-modes.html"
msgstr "https://docs.gimp.org/en/gimp-concepts-layer-modes.html"

#~ msgid "For most of Krita's mystery blendingmodes:"
#~ msgstr "För de flesta av Kritas mysteriösa blandningslägen:"

#~ msgid "http://illusions.hu/effectwiki/doku.php?id=list_of_blendings"
#~ msgstr "http://illusions.hu/effectwiki/doku.php?id=list_of_blendings"
