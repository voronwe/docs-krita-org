# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-11 03:18+0200\n"
"PO-Revision-Date: 2019-06-11 18:25+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../reference_manual/dockers/onion_skin.rst:1
msgid "Overview of the onion skin docker."
msgstr "Översikt av rispapperspanelen."

#: ../../reference_manual/dockers/onion_skin.rst:10
msgid "Animation"
msgstr "Animering"

#: ../../reference_manual/dockers/onion_skin.rst:10
msgid "Onion Skin"
msgstr "Rispapper"

#: ../../reference_manual/dockers/onion_skin.rst:15
msgid "Onion Skin Docker"
msgstr "Rispapperspanel"

#: ../../reference_manual/dockers/onion_skin.rst:18
msgid ".. image:: images/dockers/Onion_skin_docker.png"
msgstr ".. image:: images/dockers/Onion_skin_docker.png"

#: ../../reference_manual/dockers/onion_skin.rst:19
msgid ""
"To make animation easier, it helps to see both the next frame as well as the "
"previous frame sort of layered on top of the current. This is called *onion-"
"skinning*."
msgstr ""
"För att göra animering lättare hjälper det att se både nästa bildruta samt "
"föregående bildruta så att säga lagrade ovanpå varandra. Det kallas "
"*rispapper*."

#: ../../reference_manual/dockers/onion_skin.rst:22
msgid ".. image:: images/dockers/Onion_skin_01.png"
msgstr ".. image:: images/dockers/Onion_skin_01.png"

#: ../../reference_manual/dockers/onion_skin.rst:23
msgid ""
"Basically, they are images that represent the frames before and after the "
"current frame, usually colored or tinted."
msgstr ""
"Egentligen är de bilderna som representerar bildrutorna före och efter "
"aktuell bildruta, oftast färgade eller tonade."

#: ../../reference_manual/dockers/onion_skin.rst:25
msgid ""
"You can toggle them by clicking the lightbulb icon on a layer that is "
"animated (so, has frames), and isn’t fully opaque. (Krita will consider "
"white to be white, not transparent, so don’t animated on an opaque layer if "
"you want onion skins.)"
msgstr ""
"Det går att visa eller dölja dem genom att klicka på lampikonen på en lager "
"som är animerat (och alltså har bildrutor), och inte är helt ogenomskinliga. "
"(Krita anser att vitt är vitt, inte genomskinligt, så animera inte på ett "
"ogenomskinligt lager om rispapper ska användas.)"

#: ../../reference_manual/dockers/onion_skin.rst:29
msgid ""
"Since 4.2 onion skins are disabled on layers whose default pixel is fully "
"opaque. These layers can currently only be created by using :guilabel:"
"`background as raster layer` in the :guilabel:`content` section of the new "
"image dialog. Just don't try to animate on a layer like this if you rely on "
"onion skins, instead make a new one."
msgstr ""
"Sedan 4.2 är rispapper inaktiverat på lager vars normala bildpunkter är helt "
"ogenomskinliga. Lagren kan för närvarande bara skapas genom att använda :"
"guilabel:`bakgrund som rastreringslager` i sektionen :guilabel:`innehåll` i "
"den nya bilddialogrutan. Försök bara inte animera på ett sådant lager om du "
"vill förlita sig på ett rispapper, skapa istället ett nytt."

#: ../../reference_manual/dockers/onion_skin.rst:31
msgid ""
"The term onionskin comes from the fact that onions are semi-transparent. In "
"traditional animation animators would make their initial animations on "
"semitransparent paper on top of an light-table (of the special animators "
"variety), and they’d start with so called keyframes, and then draw frames in "
"between. For that, they would place said keyframes below the frame they were "
"working on, and the light table would make the lines of the keyframes shine "
"through, so they could reference them."
msgstr ""
"Begreppet rispapper kommer ifrån det faktum att det är halvgenomskinligt. I "
"traditionell animering gjorde tecknarna sina originalanimeringar på "
"halvgenomskinligt papper med ett ljusbord (av den särskilda sort som "
"användes av tecknare), och de började med så kallade nyckelbilder, och "
"ritade sedan bilderna däremellan. För att göra det, placerades nämnda "
"nyckelbilder under bilden de arbetade på, och ljusbordet lät nyckelbildens "
"linjer skina igenom så att de kunde användas som referens."

#: ../../reference_manual/dockers/onion_skin.rst:33
msgid ""
"Onion-skinning is a digital implementation of such a workflow, and it’s very "
"useful when trying to animate."
msgstr ""
"Rispapper är en digital implementering av ett sådant arbetsflöde, och det är "
"mycket användbart vid försök att animera."

#: ../../reference_manual/dockers/onion_skin.rst:36
msgid ".. image:: images/dockers/Onion_skin_02.png"
msgstr ".. image:: images/dockers/Onion_skin_02.png"

#: ../../reference_manual/dockers/onion_skin.rst:37
msgid ""
"The slider and the button with zero offset control the master opacity and "
"visibility of all the onion skins. The boxes at the top allow you to toggle "
"them on and off quickly, the main slider in the middle is a sort of ‘master "
"transparency’ while the sliders to the side allow you to control the "
"transparency per keyframe offset."
msgstr ""
"Skjutreglaget och knappen med nollposition bestämmer huvudogenomskinligheten "
"och synligheten för alla rispapper. Rutorna längst upp gör att man kan "
"stänga av och sätta på dem snabbt, huvudreglaget i mitten är en sorts "
"'huvudgenomskinlighet' medan reglagen vid sidan gör det möjligt att "
"kontrollera genomskinligheten per nyckelbild."

#: ../../reference_manual/dockers/onion_skin.rst:39
msgid ""
"Tint controls how strongly the frames are tinted, the first screen has 100%, "
"which creates a silhouette, while below you can still see a bit of the "
"original colors at 50%."
msgstr ""
"Toningen bestämmer hur starkt rutorna tonas, där den första skärmen har 100 "
"%, vilket skapar en siluett, medan man fortfarande kan se en del av "
"originalfärgerna nedanför med 50 %."

#: ../../reference_manual/dockers/onion_skin.rst:41
msgid ""
"The :guilabel:`Previous Frame` and :guilabel:`Next Frame` color labels "
"allows you set the colors."
msgstr ""
":guilabel:`Föregående bildruta` och :guilabel:`Nästa bildruta` låter dig "
"ställa in färgerna."
