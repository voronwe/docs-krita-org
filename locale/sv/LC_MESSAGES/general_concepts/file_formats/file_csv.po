# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-07-30 20:32+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../general_concepts/file_formats/file_csv.rst:1
msgid "The CSV file format as exported by Krita."
msgstr "CSV-filformatet som exporteras av Krita."

#: ../../general_concepts/file_formats/file_csv.rst:10
msgid "*.csv"
msgstr "*.csv"

#: ../../general_concepts/file_formats/file_csv.rst:10
msgid "CSV"
msgstr "CSV"

#: ../../general_concepts/file_formats/file_csv.rst:10
msgid "Comma Separated Values"
msgstr "Värden åtskilda med kommatecken"

#: ../../general_concepts/file_formats/file_csv.rst:15
msgid "\\*.csv"
msgstr "\\*.csv"

#: ../../general_concepts/file_formats/file_csv.rst:17
msgid ""
"``.csv`` is the abbreviation for Comma Separated Values. It is an open, "
"plain text spreadsheet format. Since the CSV format is a plain text itself, "
"it is possible to use a spreadsheet program or even a text editor to edit "
"the ``*.csv`` file."
msgstr ""
"Förkortningen ``.csv`` betyder Comma Separated Values (värden åtskilda med "
"kommatecken). Det är ett öppet, enkelt textformat för kalkylblad. Eftersom "
"själva .csv-formatet är enkel text, är det möjligt att använda ett "
"kalkylprogram eller till och med en texteditor för att redigera ``.csv``-"
"filen."

#: ../../general_concepts/file_formats/file_csv.rst:19
msgid ""
"Krita supports the CSV version used by TVPaint, to transfer layered "
"animation between these two softwares and probably with others, like "
"Blender. This is not an image sequence format, so use the document loading "
"and saving functions in Krita instead of the :guilabel:`Import animation "
"frames` and :guilabel:`Render Animation` menu items."
msgstr ""
"Krita stöder versionen av CSV som används av TVPaint för att överföra "
"lagerinformation mellan de två programvarorna och troligen också med andra, "
"såsom Blender. Det är inte ett bildsekvensformat, så använd funktionerna för "
"att läsa in och spara dokument i Krita istället för menyalternativen :"
"guilabel:`Importera animeringsbildrutor` och :guilabel:`Återge animering`."

#: ../../general_concepts/file_formats/file_csv.rst:21
msgid ""
"The format consists of a text file with ``.csv`` extension, together with a "
"folder under the same name and a ``.frames`` extension. The CSV file and the "
"folder must be on the same path location. The text file contains the "
"parameters for the scene, like the field resolution and frame rate, and also "
"contains the exposure sheet for the layers. The folder contains :ref:"
"`file_png` picture files. Unlike image sequences, a key frame instance is "
"only a single file and the exposure sheet links it to one or more frames on "
"the timeline."
msgstr ""
"Formatet består av en textfil med filändelsen ``.csv``, tillsammans med en "
"katalog med samma namn och filändelsen ``.frames``. Både CSV-filen och "
"katalogen måste finnas på samma ställe. Textfilen innehåller "
"scenparametrarna, som fältupplösningen och bildhastigheten, och innehåller "
"också exponeringsbladet för lagren. Katalogen innehåller :ref:`file_png` "
"bildfiler. I motsats till bildsekvenser, är en nyckelbildruta bara en "
"enstaka fil och exponeringsbladet länkar den till en eller flera bildrutor "
"på tidslinjen."

#: ../../general_concepts/file_formats/file_csv.rst:26
msgid ".. image:: images/Csv_spreadsheet.png"
msgstr ".. image:: images/Csv_spreadsheet.png"

#: ../../general_concepts/file_formats/file_csv.rst:26
msgid "A ``.csv`` file as a spreadsheet in :program:`LibreOffice Calc`."
msgstr "En ``.csv``-fil som kalkylark i :program:`LibreOffice Calc`"

#: ../../general_concepts/file_formats/file_csv.rst:28
msgid ""
"Krita can both export and import this format. It is recommended to use 8bit "
"sRGB color space because that's the only color space for :program:`TVPaint`. "
"Layer groups and layer masks are also not supported."
msgstr ""
"Krita kan både exportera och importera formatet. Det rekommenderas att "
"använda 8-bitars sRGB färgrymder eftersom det är den enda färgrymden i :"
"program:`TVPaint`. Lagergrupper och lagermasker stöds inte heller."

#: ../../general_concepts/file_formats/file_csv.rst:30
msgid ""
"TVPaint can only export this format by itself. In :program:`TVPaint 11`, use "
"the :guilabel:`Export to...` option of the :guilabel:`File` menu, and on the "
"upcoming :guilabel:`Export footage` window, use the :guilabel:`Clip: Layers "
"structure` tab."
msgstr ""
"TVPaint själv kan bara exportera formatet. Använd alternativet :guilabel:"
"`Export to...` i menyn :guilabel:`File` i :program:`TVPaint 11`, och använd "
"fliken :guilabel:`Clip: Layers structure` i fönstret :guilabel:`Export "
"footage` som visas."

#: ../../general_concepts/file_formats/file_csv.rst:35
msgid ".. image:: images/Csv_tvp_csvexport.png"
msgstr ".. image:: images/Csv_tvp_csvexport.png"

#: ../../general_concepts/file_formats/file_csv.rst:35
msgid "Exporting into ``.csv`` in TVPaint."
msgstr "Exportera till ``.csv`` i TVPaint."

#: ../../general_concepts/file_formats/file_csv.rst:37
msgid ""
"To import this format back into TVPaint there is a George language script "
"extension. See the \"Packs, Plugins, Third party\" section on the TVPaint "
"community forum for more details and also if you need support for other "
"softwares. Moho/Anime Studio and Blender also have plugins to import this "
"format."
msgstr ""
"För att importera tillbaka formatet till TVPaint finns en skriptutökning i "
"språket George. Se avsnittet \"Packs, Plugins, Third party\" i TVPaints "
"gemenskapsforum för mer information, och även om du behöver stöd för andra "
"programvaror. Moho/Anime Studio och Blender har också insticksprogram för "
"att importera formatet."

#: ../../general_concepts/file_formats/file_csv.rst:42
msgid ""
"`CSV import script for TVPaint <https://forum.tvpaint.com/viewtopic.php?"
"f=26&t=9759>`_."
msgstr ""
"`CSV-importskript för TVPaint <https://forum.tvpaint.com/viewtopic.php?"
"f=26&t=9759>`_"

#: ../../general_concepts/file_formats/file_csv.rst:43
msgid ""
"`CSV import script for Moho/Anime Studio <https://forum.tvpaint.com/"
"viewtopic.php?f=26&t=10050>`_."
msgstr ""
"`CSV-importskript för Moho/Anime Studio <https://forum.tvpaint.com/viewtopic."
"php?f=26&t=10050>`_"

#: ../../general_concepts/file_formats/file_csv.rst:44
msgid ""
"`CSV import script for Blender <https://developer.blender.org/T47462>`_."
msgstr "`CSV-importskript för Blender <https://developer.blender.org/T47462>`_"
